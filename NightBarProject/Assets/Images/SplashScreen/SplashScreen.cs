﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour {

    AsyncOperation async;
    bool endSplash = false;

    // Use this for initialization
    void Start()
    {
        PotaTween popTween = PotaTween.Create(gameObject);
        popTween.SetScale(Vector3.zero, Vector3.one * .7f);
        popTween.SetEaseEquation(Ease.Equation.OutBounce);
        popTween.SetDuration(0.5f);

        popTween.Play(StartLoad);
    }

    void StartLoad()
    {
        StartCoroutine("load");
    }

    IEnumerator load()
    {
        Debug.LogWarning("ASYNC LOAD STARTED - " +
           "DO NOT EXIT PLAY MODE UNTIL SCENE LOADS... UNITY WILL CRASH");
        async = Application.LoadLevelAsync(Application.loadedLevel + 1);
        async.allowSceneActivation = false;
        yield return async;


    }
    
    // Update is called once per frame
    void Update () {
        if (async != null)
        {
            if (async.progress > 0.8 && endSplash == false)
            {
                EndSplash();
                endSplash = true;
            }
        }
    }

    public void EndSplash()
    {
        PotaTween alphaTween = PotaTween.Create(gameObject,1);
        alphaTween.SetAlpha(1f, 0f);
        alphaTween.SetDuration(4f);
        alphaTween.SetEaseEquation(Ease.Equation.InSine);

        alphaTween.Play(EndScene);
    }

    private void EndScene()
    {
        async.allowSceneActivation = true;
    }
}
