﻿using System;
using UnityEngine;

    public class Future<T> : Future
    {
        public Future()
        {
        }
 
        public Future(Action callbackAction, float timeout = 0, Action timeoutAction = null)
        {
            InitFuture(callbackAction, timeout, timeoutAction);
        }

        public static implicit operator T(Future<T> f)
        {
            return f.Value;
        }

        #region Return Value
        private bool _hasValue = false;
        public bool HasValue
        {
            get { return _hasValue; }
        }

        private T _value;
        public T Value
        {
            get
            {
                return _value;
            }
            set
            {
                _hasValue = true;
                _value = value;
                SetAsFinished();
            }
        }
        #endregion
    }

    /// <summary>
    /// Helper class to manage async operations
    /// </summary>
    public class Future
    {
        private Action _callbackAction;
        private Action _timeoutAction;

        public Future(Action callbackAction = null, float timeout = 0, Action timeoutAction = null)
        {
            InitFuture(callbackAction, timeout, timeoutAction);
        }

        /// <summary>
        /// Allows future reuse
        /// </summary>
        /// <param name="callbackAction"></param>
        /// <param name="timeout"></param>
        /// <param name="timeoutAction"></param>
        public void Reset(Action callbackAction = null, float timeout = 0, Action timeoutAction = null)
        {
            Exception = null;
            InitFuture(callbackAction, timeout, timeoutAction);
        }
        /// <summary>
        /// Initiates fields.
        /// Used a protected method instead of a protected field for these reasons: http://stackoverflow.com/questions/3182653/are-protected-members-fields-really-that-bad
        /// </summary>
        /// <param name="callbackAction"></param>
        /// <param name="timeout"></param>
        /// <param name="timeoutAction"></param>
        protected void InitFuture(Action callbackAction, float timeout = 0, Action timeoutAction = null)
        {
            _isFinished = false;
            _callbackAction = callbackAction;
            _timeout = timeout;
            _startTime = Time.unscaledTime;
            _timeoutAction = timeoutAction;
        }

        #region Flow Management
        private bool _isFinished = false;
        /// <summary>
        /// This field can be used for Futures that don't a return value.
        /// Upon finishing
        /// </summary>
        public bool IsFinished
        {
            get { return _isFinished || HasTimeouted || HasException || _canceled; }
        }

        public bool IsSuccessful
        {
            get { return _isFinished && !HasTimeouted && !HasException && !_canceled; }
        }

        public void SetAsFinished()
        {
            if (IsFinished)
                return;
            
            _isFinished = true;
            if (_callbackAction != null)
                _callbackAction.Invoke();
        }

        private bool _canceled;

        public bool IsCanceled
        {
            get { return _canceled; }
        }
        public void CancelFuture()
        {
            _canceled = true;
        }
        #endregion
        
        #region Exception
        public bool HasException
        {
            get { return Exception != null; }
        }

        public Exception Exception;
        #endregion

        #region Timeout
        private float _timeout;
        private float _startTime = -1;
        public bool HasTimeouted
        {
            get
            {
                if (_timeout <= 0 || Time.unscaledTime < _timeout + _startTime)
                    return false;

                if (_timeoutAction != null)
                    _timeoutAction.Invoke();

                _isFinished = true;
                return true;
            }
        }
        #endregion
    }
