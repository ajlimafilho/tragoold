﻿using UnityEngine;
using System.Collections;

public class GameJoltManager : MonoBehaviour {

    public static string CONTINUE_KEY = "";
    public static int CONTINUE_COUNT = 0;

    public int continues;
    public GameObject checkTime;
    public GameObject loadingBar;
    public GameObject gameJolt;

    private bool tabOn = false;

    void Start()
    {

        bool isSignedIn = GameJolt.API.Manager.Instance.CurrentUser != null;

        if (isSignedIn)
        {
            checkTime.SetActive(true);
            return;
        }

        GameJolt.UI.Manager.Instance.ShowSignIn((bool success) =>
        {

            if (success)
            {
                checkTime.SetActive(true);
            }
            else
            {
                Application.Quit();
            }

        });
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            //tabOn = true;
            //GameManager.GJON = GameManager.VERSION_OFF;
            //GameJolt.UI.Manager
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (gameJolt.activeInHierarchy && tabOn && Input.GetKeyDown(KeyCode.G))
        {
            gameJolt.SetActive(false);
            loadingBar.SetActive(true);
            loadingBar.SendMessage("StartLoading", 1);
        }
    }
}
