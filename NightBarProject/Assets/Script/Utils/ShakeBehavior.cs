﻿using UnityEngine;
using System.Collections;

public class ShakeBehavior : MonoBehaviour {

    public float beginShakeAmount = 0.1f;
    public float apexShakeAmount = 0.2f;
    public bool startShaking = false;

    private Transform ownTransform;
    private float shakeTime;

    private float timePassed;

    private bool isShaking = false;
    private bool noTime = false;
    private Vector3 savesPos;

    void Awake () {
        ownTransform = transform;

        if (startShaking)
        {
            StartShake();
        }
    }
    
    void FixedUpdate () {

        if (isShaking)
        {
            DoShake();
            isShaking = (timePassed < shakeTime) || noTime;

            if (!isShaking)
            {
                ownTransform.localPosition = savesPos;
            }
        }

        timePassed += Time.fixedDeltaTime;
    }

    private void DoShake()
    {
        Vector3 newPosition = savesPos+ (Random.insideUnitSphere * Ease.InOutSine(timePassed, beginShakeAmount, apexShakeAmount, shakeTime));
        newPosition.z = savesPos.z;

        ownTransform.localPosition = newPosition;
    }

    public void Shake(float sec)
    {
        if (isShaking) return;

        isShaking = true;
        shakeTime = sec;
        timePassed = 0;
        noTime = false;
        savesPos = ownTransform.localPosition;
    }

	public void DelayStartShake(float time)
	{
		Invoke ("StartShake", time);
	}

    public void StartShake()
    {
        if (isShaking) return;

        isShaking = true;
        noTime = true;
        shakeTime = float.MaxValue;
        timePassed = 0;
        savesPos = transform.localPosition;
    }

    public void StopShake()
    {

        isShaking = false;
        noTime = false;
    }
}
