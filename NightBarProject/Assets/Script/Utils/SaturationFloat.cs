﻿using UnityEngine;
using System.Collections;

public class SaturationFloat : MonoBehaviour {

    public Vector2 range;

    private Material HSVMaterial;
    private PotaTween saturationTween;
    

    void Awake () {

        HSVMaterial = GetComponent<SpriteRenderer>().material;

        saturationTween = PotaTween.Create(gameObject,2);
        saturationTween.SetFloat(range.x, range.y);
        saturationTween.UpdateCallback(ApplyToMaterial);
        saturationTween.SetLoop(LoopType.PingPong);
        saturationTween.SetEaseEquation(Ease.Equation.InElastic);
        saturationTween.SetDuration(5);
        saturationTween.Play();
    }

    private void ApplyToMaterial()
    {
        HSVMaterial.SetFloat("_Sat", saturationTween.Float.Value);
    }
}
