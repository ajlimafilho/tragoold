﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StateSprite : MonoBehaviour
{
    #region Messages
    public const string CHANGE_STATE = "ChangeState";
    #endregion

    #region Properties
    public int CurrentStateIndex
    {
        get
        {
            return currentIndex;
        }
    }
    public bool NoState
    {
        get
        {
            return noState;
        }
    }
    #endregion

    public List<Sprite> states;
    public int currentIndex = 0;

    private SpriteRenderer renderer;
    private bool noState = false;
    private Sprite baseSprite;
	
	void Awake () 
    {
        renderer = GetComponent<SpriteRenderer>();

        if (renderer == null)
        {
            renderer = gameObject.AddComponent<SpriteRenderer>();
        }

        baseSprite = renderer.sprite;
	}

    void Start()
    {
        if (states.Count > 0)
        {
           // ChangeState(currentIndex);
        }
    }

    public void ChangeState(int index)
    {
        index = Mathf.Clamp(index, 0, states.Count - 1);
        renderer.sprite = states[index];
        currentIndex = index;
    }

    public void ClearState()
    {
        renderer.sprite = baseSprite;
        noState = true;
    }

    public void NextState()
    {
        currentIndex++;

        if (currentIndex >= states.Count)
        {
            currentIndex = 0;
        }

        renderer.sprite = states[currentIndex];
    }
	
}
