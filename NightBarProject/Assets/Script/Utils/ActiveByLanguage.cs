﻿using UnityEngine;
using System.Collections;

public class ActiveByLanguage : MonoBehaviour {

    public GameObject PTObject;
    public GameObject ENObject;

    void Awake () 
    {
        if (StringTable.deviceLanguage == SystemLanguage.English)
        {
            ENObject.SetActive(true);
            return;
        }

        PTObject.SetActive(true);
    }
    
    void Update () 
    {
    
    }
}
