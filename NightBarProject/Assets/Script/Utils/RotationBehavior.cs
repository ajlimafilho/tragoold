﻿using UnityEngine;
using System.Collections;

public class RotationBehavior : MonoBehaviour {

    public float speed = 0f;
    public bool playWhenStart = true;
    public bool startRandomLocation = false;

    private bool active;
    private Transform ownTransform;

    void Start () {
        ownTransform = transform;

        active = false;
        if (playWhenStart)
        {
            active = true;
        }

        if (startRandomLocation)
        {
            ownTransform.Rotate(Vector3.forward * Random.Range(0, 360));
        }
    }
    
    void FixedUpdate () { 
        if(active)
        {
            ownTransform.Rotate(Vector3.forward * ((speed * 100) * Time.fixedDeltaTime));
        }
    }

    public void ChangeRotationSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

    public  void StopRotation()
    {
        active = false;
    }

    public void StartRotation()
    {
        active = true;
    }

}
