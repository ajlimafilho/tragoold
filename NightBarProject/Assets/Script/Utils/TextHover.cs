﻿using UnityEngine;
using System.Collections;

public class TextHover : MonoBehaviour {

    public Color basicColor;
    public Color colorHover;

    public GameAction hoverAction;
    public GameAction hoverOutAction;

    private PotaTween colorTween;

    void Start()
    {
        colorTween = PotaTween.Create(gameObject);
        ChangeColor(basicColor, colorHover);
    }

    public void ChangeColor(Color basicColor, Color colorHover)
    {
        colorTween.Clear();
        colorTween.SetColor(basicColor, colorHover);
        colorTween.SetDuration(0.2F);
    }

    void OnMouseEnter()
    {

        colorTween.Stop();
        colorTween.Play();

        if (hoverAction)
        {
            hoverAction.DoAction();
        }
    }

    void OnMouseExit()
    {

        colorTween.Stop();
        colorTween.Reverse();

        if (hoverOutAction)
        {
            hoverOutAction.DoAction();
        }
    }

    void Update () {
    
    }
}
