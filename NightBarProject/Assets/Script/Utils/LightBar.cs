﻿using UnityEngine;
using System.Collections;

public class LightBar : MonoBehaviour {

    private SpriteRenderer rederer;
    private PotaTween lightTween;

	void Start () {
        rederer = GetComponent<SpriteRenderer>();
        lightTween = PotaTween.Create(gameObject);
        lightTween.SetAlpha(0.5f, 1.0f);
        lightTween.SetLoop(LoopType.PingPong);
        lightTween.SetDuration(1f);
        lightTween.Play();
	}
	
	
	void Update () {
	    
	}
}
