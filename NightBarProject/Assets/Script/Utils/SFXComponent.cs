﻿using UnityEngine;
using System.Collections;
using FMOD.Studio;

public class SFXComponent : MonoBehaviour {

    private FMOD.Studio.EventInstance instance;
    private FMOD.Studio.EventInstance music;

    public void PlayOneShot(string name)
    {
        FMOD_StudioSystem.instance.PlayOneShot("event:/" + name, transform.position);
    }

    public void PlayMusic(string name)
    {
        music = FMOD_StudioSystem.instance.GetEvent("event:/" + name);
        music.start();
    }

    public void PlayInstance(string name)
    {
        instance = FMOD_StudioSystem.instance.GetEvent("event:/" + name);
        instance.start();
    }

    public void StopInstance()
    {
        if (instance == null) return;

        if (instance.isValid())
        {
            instance.stop(STOP_MODE.IMMEDIATE);
        }
    }

}
