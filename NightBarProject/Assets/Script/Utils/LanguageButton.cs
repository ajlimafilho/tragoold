﻿using UnityEngine;
using System.Collections;

public class LanguageButton : MonoBehaviour {

    public SystemLanguage language;
    public LoadingBar loadingBar;
    public GameObject calendarMenu;

    void Awake () {
        
    }
    
    void Update () {
    
    }

    void OnMouseDown()
    {
        StringTable.SetLanguage(language, true);

        LanguageButton[] buttons = FindObjectsOfType<LanguageButton>();

        int count = 0;

        while (count < buttons.Length)
        {
            if (buttons[count] != this)
            {
                buttons[count].gameObject.SetActive(false);
            }
            
            count++;
        }

        PotaTween centralizeTween = PotaTween.Create(gameObject,2);
        centralizeTween.SetPosition(TweenAxis.X, transform.position.x, 0);
        centralizeTween.SetDuration(0.5f);
        centralizeTween.SetAlpha(1, 0);
        centralizeTween.SetEaseEquation(Ease.Equation.OutBack);

        centralizeTween.Play(StartLoading);
        
        if (!StringTable.inited)
        {
            StringTable.LoadLanguage();
        }

        GetComponent<BoxCollider2D>().enabled = false;
    }

    private void StartLoading()
    {

        if (!WeeklySetup.WEEKLY_ON)
        {
            if (!PremiumSetup.SKIP_BASIC_SETUP)
            {
                Application.LoadLevel(4);
            }
            return;
        }

        if (ConnectionHelper.Instance)
        {
            System.DateTime dateTime = ConnectionHelper.Instance.DateTime;
            //Debug.Log(WeeklySetup.CONTINUE_COUNT == 0);

            if (!OpeningCalendar.CheckDayIsOpened(dateTime.DayOfWeek.ToString()) || (WeeklySetup.CONTINUE_COUNT == 0))
            {
                if (calendarMenu)
                {
                    calendarMenu.SetActive(true);
                }

                return;
            }
        }

        loadingBar.gameObject.SetActive(true);
        int episodeIndex = 1;

        switch (ConnectionHelper.Instance.DateTime.DayOfWeek)
        {
            case System.DayOfWeek.Thursday:
                episodeIndex = 1;
                break;
            case System.DayOfWeek.Friday:
                episodeIndex = 2;
                break;
            case System.DayOfWeek.Saturday:
                episodeIndex = 3;
                break;
            case System.DayOfWeek.Monday:
                episodeIndex = 4;

                if (GameManager.TICKET_ON)
                {
                    episodeIndex = 6;
                }

                break;
        }
        
        loadingBar.StartLoading(episodeIndex);
    }
}
