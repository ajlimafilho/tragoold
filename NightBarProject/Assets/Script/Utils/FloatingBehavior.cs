﻿using UnityEngine;
using System.Collections;

public class FloatingBehavior : MonoBehaviour {

    public bool startFloating = true;
    public float velocity = 5f;
    public float floatingHeight = 0.5f;
    public bool floatHorizontal = false;


    private float step = 0;
    private bool canFloat = false;

    private Transform ownTransform;

	void Start ()
    {
        ownTransform = transform;
        if (startFloating)
        {
            PlayFloat();
        }
	}

    public void PlayFloat()
    {
        canFloat = true;
        step = 0;

    }

    public void StopFloat()
    {
        canFloat = false;
    }
	
	void FixedUpdate () {
        if (canFloat)
        {
            step += velocity * Time.fixedDeltaTime;
            Vector3 newPosition = ownTransform.localPosition;
            float addValue = ((floatingHeight * Time.fixedDeltaTime) * Mathf.Cos(step));

            if (floatHorizontal)
            {
                newPosition.x += addValue;
            }
            else
            {
                newPosition.y += addValue;
            }

            ownTransform.localPosition = newPosition;
        }
	}
}
