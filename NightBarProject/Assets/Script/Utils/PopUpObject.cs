﻿using UnityEngine;
using System.Collections;

public class PopUpObject : MonoBehaviour
{
    private PotaTween pop;
    private PotaTween popReverse;

    void Awake()
    {
        pop = PotaTween.Create(gameObject);
        pop.SetScale(Vector3.one * .1f, Vector3.one);
        pop.SetEaseEquation(Ease.Equation.OutElastic);
        pop.SetDuration(1.5f);

        popReverse = PotaTween.Create(gameObject, 1);
        popReverse.SetScale(Vector3.one, Vector3.zero);
        popReverse.SetEaseEquation(Ease.Equation.InElastic);
        popReverse.SetAlpha(1, 0);
        popReverse.SetDuration(1f);
    }

    public void OnEnable ()
    {
        pop.Play();
    }

    public void PopReverse()
    {
        popReverse.Play(DisableObject);
    }

    void OnMouseDown()
    {
        PopReverse();
    }

    void DisableObject()
    {
        gameObject.SetActive(false);
    }
}
