﻿using UnityEngine;
using System.Collections;

public class FollowMousePos : MonoBehaviour {
    public float moveSpeed = 1f;
    Vector3 mousePosition;
    // Use this for initialization
    void Start () {
    
    }
    
    // Update is called once per frame
    void Update () {
        
            mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

            Vector3 newPos = Vector2.Lerp(transform.position, mousePosition, moveSpeed);
            newPos.z = -10;
            transform.position = newPos;
        
    }
}
