﻿using UnityEngine;
using System.Collections;

public class Bounce : MonoBehaviour {

    public float speed = 1;
    public Vector2 bounceBounds;
    public bool startBouncing = false;
    public bool changeColor = false;
    public Color32 color = new Color32(180, 180, 180, 255);

    private bool canBounce = false;
    private Transform ownTransform;
    private SpriteRenderer sprite;

    void Start () 
    {
        sprite = GetComponent<SpriteRenderer>();
        ownTransform = transform;
        canBounce = startBouncing;
    }
    
    void FixedUpdate () {
        if (canBounce)
        {
            ownTransform.localScale = Vector3.Lerp(Vector3.one * bounceBounds.x, Vector3.one * bounceBounds.y, Mathf.PingPong(Time.time, speed));
            
            if (changeColor)
            {
                sprite.color = Color.Lerp(color, Color.white, Mathf.PingPong(Time.time, speed));
            }
        }
    }

    public void StartBouncing()
    {
        canBounce = true;
    }

    public void StopBouncing()
    {
        canBounce = false;
    }
}
