﻿using UnityEngine;
using System.Collections;

public class PopText : MonoBehaviour {

    public TextMesh textMesh;

    private PotaTween entryTween;
    private PotaTween endTween;

    void Start()
    {
        SetupTweens();
    }

    public void Pop(string text)
    {
        textMesh.text = text;

        SetupTweens();
        entryTween.Play(EndIntro);
    }

    public void Pop(string text, Color color)
    {
        textMesh.text = text;
        textMesh.color = color;

        SetupTweens();
        entryTween.Play(EndIntro);
        

    }

    private void SetupTweens()
    {
        entryTween = PotaTween.Create(gameObject);
        entryTween.SetScale(Vector3.one, Vector3.one * 1.5f);
        entryTween.SetLoop(LoopType.PingPong);
        entryTween.SetEaseEquation(Ease.Equation.OutSine);
        entryTween.SetDuration(0.2f);
        entryTween.SetRotation(TweenAxis.Z, 0, Random.Range(-30, 30));

        endTween = PotaTween.Create(gameObject, 1);
        endTween.SetDelay(0.1f);
        endTween.SetEaseEquation(Ease.Equation.InSine);
        endTween.SetAlpha(1, 0f);
        endTween.SetDuration(0.2f);

        endTween.SetPosition(TweenAxis.Y, transform.position.y, transform.position.y + 0.5f);
    }

    public void EndIntro()
    {
        endTween.Play(SelfDestroy);
    }

    private void SelfDestroy()
    {
        Destroy(gameObject);
    }

    public void SetText(string text)
    {
        textMesh.text = text;
    }
}
