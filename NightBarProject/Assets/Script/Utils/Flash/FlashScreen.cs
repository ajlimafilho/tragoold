﻿using UnityEngine;
using System.Collections;

public class FlashScreen : MonoBehaviour
{
    private Texture2D pixel;
    public bool drawOnPlane = false;
    public MeshRenderer renderer;

    public Color color = Color.red;
    public float startAlpha = 0.0f;
    public float maxAlpha = 1.0f;
    public float rampUpTime = 0.5f;
    public float holdTime = 0.5f;
    public float rampDownTime = 0.5f;

    enum FLASHSTATE { OFF, UP, HOLD, DOWN }
    Timer timer;
    FLASHSTATE state = FLASHSTATE.OFF;

    // Use this for initialization
    void Start()
    {
        pixel = new Texture2D(1, 1);
        color.a = startAlpha;
        pixel.SetPixel(0, 0, color);
        pixel.Apply();

        if (drawOnPlane)
        {
            float height = Camera.main.orthographicSize * 2.0f;
            float width = height * Screen.width / Screen.height;
            renderer.transform.localScale = (new Vector3(width, 0.1f, height)) * 0.1f;
        }

        // for testing
        //TookDamage(new DamagePacket(1));
    }

    public void Update()
    {
        switch (state)
        {
            case FLASHSTATE.UP:
                if (timer.UpdateAndTest())
                {
                    state = FLASHSTATE.HOLD;
                    timer = new Timer(holdTime);
                }
                break;
            case FLASHSTATE.HOLD:
                if (timer.UpdateAndTest())
                {
                    state = FLASHSTATE.DOWN;
                    timer = new Timer(rampDownTime);
                }
                break;
            case FLASHSTATE.DOWN:
                if (timer.UpdateAndTest())
                {
                    state = FLASHSTATE.OFF;
                    timer = null;
                }
                break;
        }
    }

    private void SetPixelAlpha(float a)
    {
        color.a = a;
        pixel.SetPixel(0, 0, color);
        pixel.Apply();
    }

    public void OnGUI()
    {
        switch (state)
        {
            case FLASHSTATE.UP:
                SetPixelAlpha(Mathf.Lerp(startAlpha, maxAlpha, timer.Elapsed));
                break;
            case FLASHSTATE.DOWN:
                SetPixelAlpha(Mathf.Lerp(maxAlpha, startAlpha, timer.Elapsed));
                break;
        }

        if (!drawOnPlane)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), pixel);
            return;
        }

        if (state != FLASHSTATE.OFF)
        {
            renderer.material.mainTexture = pixel;
        }
    }

    public void StartFlash(float timeToCountInSec)
    {
        timer = new Timer(timeToCountInSec);
        state = FLASHSTATE.UP;
    }

}