﻿using UnityEngine;
using System.Collections;

public class CheatCode : MonoBehaviour {

    public GameObject checkTime;
    public GameObject loadingBar;
    public GameObject gameJolt;

    private bool tabOn = false;

    void Start () {
        //ConnectionHelper.Instance.DateTime = new System.DateTime(2018, 8, 26);

        bool isSignedIn = GameJolt.API.Manager.Instance.CurrentUser != null;

        if (isSignedIn)
        {
            checkTime.SetActive(true);
            return;
        }

        GameJolt.UI.Manager.Instance.ShowSignIn((bool success) => {
            
            if (success)
            {
                checkTime.SetActive(true);
            }
            else
            {
                Application.Quit();
            }

        });
    }
    
    void Update () {

        if(Input.GetKeyDown(KeyCode.Tab))
        {
            tabOn = true;
        }

        if (gameJolt.activeInHierarchy && tabOn && Input.GetKeyDown(KeyCode.G))
        {
            gameJolt.SetActive(false);
            loadingBar.SetActive(true);
            loadingBar.SendMessage("StartLoading",1);
        }

    }
}
