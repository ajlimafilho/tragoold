﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrinkBottle : MonoBehaviour {

	public int orderIndex;
    public SpriteRenderer sprite;

    public Sprite bottleFill;
    public Sprite bottleEmpty;
    public Sprite liquidSprite;

    public bool equiped;
    public string endingTag;

    private Transform ownTransform;
    private bool isGrabbing;
    private Vector3 basePosition;

    private Transform lastParent;

    public PotaTween scaleTween;

    private Queue<DrinkBottle> targetQueue = new Queue<DrinkBottle>();
    private GinGlass glass;

    void Awake()
    {
       
    }

    void Start () 
    {
        if(endingTag != null)
            gameObject.SetActive(EndingController.COMPLETED_ENDS.Contains(endingTag));

        ownTransform = transform;
        scaleTween = PotaTween.Create(sprite.gameObject);
        glass = GameManager.Instance.glass;

        //name = StringTable.GetText("BOTTLE."+ name.ToUpper());
    }
    
    void Update () 
    {
        if (isGrabbing)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = -2;
            ownTransform.position = mousePos;
        }

    }

    void OnMouseDown()
    {

        if (equiped) return;

        basePosition = ownTransform.position;
        isGrabbing = true;
    }

    void OnMouseUp()
    {
        if (isGrabbing)
        {
            isGrabbing = false;

            if (targetQueue.Count > 0)
            {
				SwitchBottle ();
            }

            ownTransform.position = basePosition;
        }
    }

	private void SwitchBottle()
	{
		DrinkBottle bottleTarget = targetQueue.Dequeue();

		Transform parent = ownTransform.parent;
		ownTransform.parent = bottleTarget.transform.parent;
		int orderIndexSaved = orderIndex;

		orderIndex = bottleTarget.orderIndex;
		bottleTarget.orderIndex = orderIndexSaved;

		parent.SendMessage ("SequenceChange");

		PotaTween changePos = PotaTween.Create(bottleTarget.gameObject);
		changePos.SetPosition(TweenAxis.X, bottleTarget.transform.position.x, basePosition.x);
		changePos.SetPosition(TweenAxis.Y, bottleTarget.transform.position.y, basePosition.y);

		changePos.SetDuration(0.3f);
		changePos.Play();

		if (bottleTarget.equiped)
		{
			bottleTarget.transform.parent = parent;
			EquipBottle(bottleTarget);
		}

		basePosition = bottleTarget.transform.position;
	}

    private void EquipBottle(DrinkBottle bottleTarget)
    {
        equiped = true;
        bottleTarget.equiped = false;
        
        bottleTarget.scaleTween.SetScale(Vector3.one, Vector3.one);

        bottleTarget.sprite.transform.localScale = Vector3.one;

        glass.ChangeLiquidSprite(liquidSprite);
        glass.CurrentBottle = name;

        scaleTween.Clear();
        scaleTween.SetScale(Vector3.one, Vector3.one * 1.5f);
        scaleTween.SetEaseEquation(Ease.Equation.OutSine);
        scaleTween.SetDuration(0.3f);

        scaleTween.Stop();
        scaleTween.Play();

    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "Bottle")
        {
            if (isGrabbing)
            {
                DrinkBottle bottleTarget = other.GetComponent<DrinkBottle>();
                targetQueue.Enqueue(bottleTarget);

                Debug.Log("Heree"+ targetQueue.Count);
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Bottle")
        {
            if (targetQueue.Count > 0)
            {
                targetQueue.Dequeue();
            }
        }
    }
}
