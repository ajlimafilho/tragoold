﻿using UnityEngine;
using System.Collections;

public class GameMessages {
    public const string NIGHT_END = "NightEnd";
    public const string CLOSE_PUB = "ClosePub";
	public const string NEXT_SCENE = "NextScene";
    public const string OPEN_THE_SHELF = "OpenTheShelf";
}
