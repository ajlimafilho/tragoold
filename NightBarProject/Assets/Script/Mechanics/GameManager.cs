﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;


public class GameManager : MonoBehaviour
{
    public static int HowManyDrinks = 0;

    public static bool TICKET_ON = false;
    public static bool IS_MONEY_ABLE = false;

    #region Singleton
    private static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<GameManager>();

            return instance;
        }
    }
    #endregion

    public enum GameMode
    {
        DRINK,
        INTERACTION,
        DIALOGUE
    }

    public GameMode GMode
    {
        get
        {
            return gameMode;
        }
    }

    public Font russianFont;

    public bool startRaining = false;
    public BoxCollider2D levelBounds;
    public bool startDrinking = true;
    public GameObject drinkArrow;
    public int currentDay = 1;
    public int musicIndex = 3;
    public GinGlass glass;
    public bool isFinal = false;

    public GameMode gameMode = GameMode.DRINK;

    public float levelAmbience = 0;
    private CameraBehavior mainCamera;

    private Interactable[] interactables;
    private EventInstance music;
    private EventInstance ambient;
    private EventInstance rain;
    private bool closePub = false;

    void Awake () {

        interactables = FindObjectsOfType<Interactable>();


        if (!StringTable.inited)
        {
            StringTable.LoadLanguage();
        }

        if (gameMode == GameMode.DRINK)
        {
            TurnInteractableOff();
        }

        if (isFinal)
        {
            GameManager.TICKET_ON = false;
            GameManager.IS_MONEY_ABLE = false;
        }
    }

    IEnumerator StartNET()
    {
        WWW www = new WWW("http://www.timeapi.org/utc/now");
        yield return www;
        /*Debug.Log(www.data);

        Debug.Log(Network.TestConnection());

        Debug.Log(System.TimeZone.CurrentTimeZone.GetUtcOffset(System.DateTime.Now));*/
    }

    void Start()
    {
        if (WeeklySetup.GAME_JOLT && WeeklySetup.WEEKLY_ON)
        {
            GameJolt.API.Sessions.Open();
        }

        rain = FMOD_StudioSystem.instance.GetEvent("event:/ambienciaMusica/chuva");
        
        if (startRaining)
        {
            rain.start();
        }

        mainCamera = Camera.main.GetComponent<CameraBehavior>();

        Debug.Log("Level Bounds"+ levelBounds);

        if (levelBounds)
        {
            mainCamera.SetLevelBounds(levelBounds.bounds);
        }

        ambient = FMOD_StudioSystem.instance.GetEvent("event:/ambienciaMusica/ambiente");
        ambient.setParameterValue("tension", levelAmbience);
        ambient.setParameterValue("time", 0);
        ambient.setParameterValue("dia", musicIndex);

        music = FMOD_StudioSystem.instance.GetEvent("event:/ambienciaMusica/musica");
        music.setParameterValue("dia", musicIndex);

        
        ambient.start();
        music.start();

        StartCoroutine(StartNET());
    }

    public void TicketOn()
    {
        GameManager.TICKET_ON = true;

    }

    public void PlayInteration(Interactable interetable)
    {

    }

    public void ChangeTension(float value)
    {
        ambient.setParameterValue("tension", value);
    }

    public void StopCurrentMusic()
    {
        music.stop(STOP_MODE.IMMEDIATE);
    }

    public void PlayCurrentMusic()
    {
        music.start();
    }

    /*public void TurnOnSilence()
    {
        music.setParameterValue("silence", 1);
    }*/

    public void AmbienceClockSound(float time)
    {
        ambient.setParameterValue("time", time);
    }

    internal void DrinkMode()
    {
        gameMode = GameMode.DRINK;
        mainCamera.SendMessage("ActiveBlur");



        FadeDrink();

        TurnInteractableOff();
    }

    private void CloseDrinkingGame()
    {
        InteractableMode(true);
    }

    internal void DialogueMode(DialogueBox box)
    {
        gameMode = GameMode.DIALOGUE;
        FadeDrink();

        Interactable telephone = GetInteractableByName("Telephone");
        if (telephone != null)
        {
            telephone.SendMessage("CloseMenu");
        }

        TurnInteractableOff();
        currentBox = box;
    }

    public void CloseDialogueMode()
    {
        if (currentBox)
        {
            currentBox.Hide();
            ClearCurrentBox();
            //TurnInteractableOn();
        }
    }

    public void Update()
    {
    }

    public void TurnInteractableOn(bool glow = false)
    {

        foreach (Interactable interactable in interactables)
        {
            if (interactable.IsSelected)
            {
                interactable.SendMessage(InteractableMessages.HIGH_LIGHT_OFF);
            }

            interactable.TurnOn(glow);
        }
    }

    public void TurnInteractableOff()
    {
        foreach (Interactable interactable in interactables)
        {
            if (!interactable.IsSelected)
            {
                interactable.TurnOff();
            }
        }
    }

    internal void InteractableMode(bool glow = false)
    {
        gameMode = GameMode.INTERACTION;
        mainCamera.SendMessage("DeactiveBlur");

        if (closePub)
        {
            Invoke("ClosePub", 4f);
            return;
        }

        TurnInteractableOn(glow);
    }

    internal void ClosePub()
    {
        if (gameMode == GameMode.DIALOGUE)
        {
            closePub = true;
            return;
        }

        SendMessage(GameMessages.NIGHT_END);
    }

    internal void DrunkEnd()
    {
        TurnInteractableOff();
        closePub = true;
        //Invoke("ClosePub", 3.5f);
    }

    internal void NightEnd()
    {
        if (rain != null && startRaining)
        {
            rain.stop(STOP_MODE.ALLOWFADEOUT);
        }

        if (gameMode == GameMode.DRINK)
        {
            TurnInteractableOff();
            glass.SendMessage("LostCombo");
        }

        foreach (Interactable interactable in interactables)
        {
            interactable.SendMessage(GameMessages.NIGHT_END);
        }

        foreach (DialogueBox box in FindObjectsOfType<DialogueBox>())
        {
            box.Hide();
        }
    }

    public void GoToFinalScreen()
    {
        GameManager.Instance.StopMusic();
        ambient.stop(STOP_MODE.ALLOWFADEOUT);
        Application.LoadLevel(7);
    }

    public void ChangeScene(int dayIndex)
    {
        Application.LoadLevel(dayIndex);
    }

    public void NextScene()
    {
        if (isFinal)
        {
            Application.LoadLevel(7);
            return;
        }

        if (TICKET_ON && currentDay == 3)
        {
            Application.LoadLevel(6);
            TICKET_ON = false;
            return;
        }

        Application.LoadLevel(currentDay + 1);
    }

    public void StopMusic()
    {
        music.stop(STOP_MODE.ALLOWFADEOUT);
    }

    public DialogueBox currentBox { get; set; }

    internal void ClearCurrentBox()
    {
        currentBox = null;
    }

    public Interactable GetInteractableByName(string name)
    {
        foreach (Interactable interactable in interactables)
        {
            if (interactable.name == name)
            {
                return interactable;
            }
        }

        return null;
    }

    internal void RestartGame()
    {
        Application.LoadLevel(currentDay);
    }

    private void FinishNight()
    {
    }

    private void FadeDrink()
    {
        if (drinkArrow == null) return;

        if (drinkArrow.activeInHierarchy || drinkArrow != null)
        {
            PotaTween tween = PotaTween.Create(drinkArrow,2);
            tween.SetAlpha(1, 0);
            tween.SetDuration(0.5f);
            tween.Play(DeactiveDrink);
        }

    }

    private void DeactiveDrink()
    {
        if (drinkArrow == null) return;

        drinkArrow.SetActive(false);
    }

    internal static bool IsWeeklyVersion()
    {
        return WeeklySetup.WEEKLY_ON;
    }
}
