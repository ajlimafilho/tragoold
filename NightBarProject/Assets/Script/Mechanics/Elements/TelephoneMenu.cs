﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TelephoneMenu : MonoBehaviour {

    public List<Interactable> callButtons;
    public ParticleSystem emitter;
    

    private List<PotaTween> buttonTween = new List<PotaTween>();
    private int tragos = 0;

    private bool menuOpen = false;

    void Awake () 
    {
        int count = 0;
        foreach (Interactable interactable in callButtons)
        {
            buttonTween.Add(PotaTween.Create(interactable.gameObject, 3));
            buttonTween[count].SetScale(Vector3.zero, Vector3.one);
            buttonTween[count].SetDuration(0.2f);
            buttonTween[count].SetDelay(0.1f * count);

            count++;
        }
    }

    void OnMouseDown()
    {
        if (!menuOpen)
        {
            OpenMenu();
            return;
        }

        CloseMenu();
    }

    public void OpenMenu()
    {
        if (GetComponent<Interactable>().dialogueBox != null || menuOpen) return;

        int count = 0;
        foreach (PotaTween tween in buttonTween)
        {
            menuOpen = true;

            int tragosToUnlock = int.Parse(callButtons[count].GetComponent<TextMesh>().text);

            bool canShow = tragosToUnlock >= 0;

            if (canShow)
            {
                callButtons[count].gameObject.SetActive(true);

                float finalAlpha = 0.5f;

                bool canUnlock = tragosToUnlock <= tragos;

                callButtons[count].GetComponent<BoxCollider2D>().enabled = canUnlock;

                tween.SetColor(Color.white, Color.grey);

                if (canUnlock)
                {
                    finalAlpha = 1;
                    tween.SetColor(Color.grey, Color.white);
                }

                tween.SetAlpha(0.0f, finalAlpha);
                tween.Play();
            }

            count++;
        }
    }

    public void SetTragoCount(int tragos)
    {
        bool unlockNumber = false;

        if (this.tragos >= tragos) return;

        foreach (Interactable interactable in callButtons)
        {
            int tragosToUnlock = int.Parse(interactable.GetComponent<TextMesh>().text);
            //Debug.Log(tragosToUnlock + " - " + tragos);

            if(tragosToUnlock <= tragos)
            {
                
                unlockNumber = true;
            }
        }

        if (unlockNumber)
        {
            this.tragos = Mathf.Max(this.tragos, tragos);
            emitter.Play();
            OpenMenu();
        }
    }

    public void CloseMenu()
    {
        if (menuOpen)
        {
            menuOpen = false;

            foreach (Interactable interactable in callButtons)
            {
                interactable.SetSeletion(false);
                interactable.SendMessage("Stop");
                interactable.transform.localScale = Vector3.zero;
            }
        }
    }

    internal void NightEnd()
    {
        CloseMenu();
    }
    
    void Update () {
    
    }
}
