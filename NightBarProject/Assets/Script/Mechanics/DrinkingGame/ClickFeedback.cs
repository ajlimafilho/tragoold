﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ClickFeedback : MonoBehaviour {

    public GameObject prefab;
    public List<string> textList;

    void Start()
    {

    }

    private void OnClickIn(ClickInfo info)
    {
        CreateFeedback(info, "Glup!");
    }

    private void CreateFeedback(ClickInfo info, string text)
    {
        GameObject feedback = Instantiate(prefab) as GameObject;
        Destroy(feedback.gameObject, 1f);
        feedback.transform.parent = transform;

        Vector3 newPos = info.MousePos;
        newPos.z = -2;
        newPos.x = Random.Range(newPos.x - 0.5f, newPos.x + 0.5f);
        newPos.y = Random.Range(newPos.y - 0.5f, newPos.y + 0.5f);

        feedback.transform.position = newPos;

        feedback.GetComponent<PopText>().Pop(text);
    }

    private void OnClickOut(ClickInfo info)
    {
        CreateFeedback(info, "Better Stop");
    }
}
