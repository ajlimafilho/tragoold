﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class GinGlass : MonoBehaviour, IPausable
{

    public const float KEY_OFFSET = 0.85F;
    private const float TIME = 8.0F;

    public int MaxSequenceCombo
    {
        get
        {
            return maxSequenceCombo;
        }
    }

    public int MaxCombo
    {
        get
        {
            return maxTragoCombo;
        }
    }

    public GameObject CurrentKey
    {
        get
        {
            return keys[sequenceStep];
        }
    }

    public string CurrentBottle
    {
        get
        {
            return currentBottle;
        }

        set
        {
            currentBottle = value;
        }
    }

    public GameObject retractGamePoint;
    public GameObject clickGameContainer;
    public BlurOptimized blur;
    public TextMesh textCombo;
    public TextMesh todaysTrago;
    public AnimationCurve comboCurve;
    public Text uiTragoCounter;

    public TextMesh sequenceComboText;
    public TextMesh textTimer;
    public Vector2 rotLimit;
    public Transform liquidDrink;
    public CloseDrinkGame closeDrinkGameButton;

    public Transform keysContainer;
    public GameObject keyModel;
    public KeyTimingBall keyBall;
    public GameObject keyboardIcon;

    public SpriteRenderer glassSprite;

    public DrunkController drunkController;
    public GameAction fallDrunkAction;

    float liquidPosition;

    private PotaTween rotTween;
    private PotaTween takeTween;

    public float unitPerSec = 1;

    private Transform ownTransform;
    private float distanceRot;
    private bool refillTime = false;
    public bool finalDrink = false;
    public bool jucaCanFallDrunk;
    private int minimumComboToJucaFall = 7;

    private float speedAnim = 2f;
    private float speedMin = 0.7f;
    private bool pause = false;

    private float timer;
    private int combo = 0;

    private PotaTween openCombo;
    private PotaTween popCombo;
    private PotaTween popTimer;

    private string currentBottle = "Gim";

    private PotaTween gameContainerTween;

    private bool stopGameplay = true;
    private bool startGame = false;

    public Animator jucaAnimator;
    private int finalCombo;
    private int maxTragoCombo;
    private Animator doubleGlassVision;
    private int drunkAnimation = 0;
    public JucaScene5CustomActions jucaScene5;

    private List<KeyCode> sequence;
    private int sequenceLimit = 1;
    private int sequenceStep = 1;
    private List<GameObject> keys;
    private List<PotaTween> colorList;
    private float energyPercent = 0;
    private int sequenceComboCount = 0;
    private int maxSequenceCombo = 0;

    private PotaTween liquidTween;

    private Vector3 comboPosition;

    private Interactable telephone;
    private int prevStep = 0;
    private PotaTween keyContainerOut;
    private int tragoCounter = 0;

    void Awake()
    {
        ownTransform = transform;
        rotTween = PotaTween.Create(this.gameObject);
        takeTween = PotaTween.Create(ownTransform.parent.gameObject, 2);
        DecreaseLiquid();

        comboPosition = textCombo.transform.position;

        popTimer = PotaTween.Create(textTimer.transform.parent.gameObject, 2);
        popTimer.SetScale(textTimer.transform.parent.transform.localScale * 1.2f, textTimer.transform.parent.transform.localScale);
        popTimer.SetEaseEquation(Ease.Equation.OutBack);
        popTimer.SetDuration(0.2f);

        popCombo = PotaTween.Create(textCombo.gameObject, 2);
        popCombo.SetScale(Vector3.one * 0.8f, Vector3.one);
        popCombo.SetRotation(TweenAxis.Z, 30, 0);
        popCombo.SetEaseEquation(Ease.Equation.InOutBack);
        popCombo.SetDuration(0.3f);

        openCombo = PotaTween.Create(textCombo.gameObject);
        openCombo.SetScale(Vector3.zero, Vector3.one);
        openCombo.SetAlpha(0.0F, 1.0F);
        openCombo.SetRotation(TweenAxis.Z, 180, 0);
        openCombo.SetEaseEquation(Ease.Equation.OutBack);
        openCombo.SetDuration(0.3f);

        gameContainerTween = PotaTween.Create(clickGameContainer);
        gameContainerTween.SetScale(Vector3.one, Vector3.zero);
        gameContainerTween.SetPosition(clickGameContainer.transform.position, retractGamePoint.transform.position);
        gameContainerTween.SetDuration(0.4F);
        gameContainerTween.SetEaseEquation(Ease.Equation.InSine);
        gameContainerTween.SetRotation(TweenAxis.Z, 0, 60);

        liquidTween = PotaTween.Create(liquidDrink.gameObject);


        blur.blurSize = 0.5f;

        doubleGlassVision = GetComponentInParent<Animator>();

        CreateSequence();

        keyContainerOut = PotaTween.Create(keysContainer.gameObject, 3);

        keyContainerOut.SetPosition(TweenAxis.Y, keysContainer.position.y, keysContainer.position.y - 3);
        keyContainerOut.SetDuration(1F);
        keyContainerOut.SetEaseEquation(Ease.Equation.InBack);
    }

    private void StartSequence()
    {
        sequenceStep = 0;
        sequenceLimit = 1;

        PotaTween[] tweens = keys[sequenceStep].GetComponentsInChildren<PotaTween>();

        int count = 0;

        while (count < tweens.Length)
        {
            tweens[count].Play();
            count++;
        }

        textTimer.color = Color.black;
        sequenceStep = 0;
        Vector2 keySize = CurrentKey.transform.Find("frame").GetComponent<SpriteRenderer>().bounds.size;
        keyBall.Reset();

        /*Debug.Log(keys[sequenceStep].GetComponentsInChildren<PotaTween>().Length);*/
        /*keys[sequenceStep].SendMessage("Play");
        keys[sequenceLimit].SendMessage("Stop");*/

    }

    private void NextSequenceLevel()
    {
        sequenceLimit++;

        sequenceLimit = Mathf.Min(sequenceLimit, keys.Count - 1);

        keys[sequenceLimit].SetActive(true);

        keyContainerOut.Play(NewKeyAdded);

    }

    private void NewKeyAdded()
    {
        /*PotaTween[] tween = keys[sequenceStep].GetComponents<PotaTween>();
        for (int i = 0; i < tween.Length; i++)
        {
            if (tween[i].Tag != "Feedback")
                Destroy(tween[i]);
        }*/

        /*keys[sequenceStep].SendMessage("Stop");
        keys[sequenceStep].SendMessage("ReverseWithTag", "Feedback");*/

        //keys[sequenceStep].GetComponentInChildren<PotaTween>().Reverse();
        CentralizeSequence();

        Invoke("ResetSequenceTween", 0.2f);
    }

    private void ResetSequenceTween()
    {
        keys[sequenceStep].SendMessage("Stop");
        sequenceStep = 0;

        //keys[sequenceStep].GetComponentInChildren<PotaTween>().Play();
        //keys[sequenceStep].SendMessage("PlayWithTag", "Feedback");
    }

    private void NextSequenceStep()
    {
        NextKeyStart();
        UpdateSequenceCounter();
        Drink();

        Vector2 keySize = CurrentKey.transform.Find("frame").GetComponent<SpriteRenderer>().bounds.size;
        keyBall.Setup(CurrentKey, keySize);

        /*PotaTween[] tweens = keys[sequenceStep].GetComponentsInChildren<PotaTween>();

        int count = 0;

        while (count < tweens.Length)
        {
            tweens[count].Stop();
            tweens[count].Reset();

            count++;
        }*/

        /*keys[sequenceStep].SendMessage("Stop");
        keys[sequenceStep].SendMessage("ReverseWithTag","Feedback");*/
        Invoke("DeactiveTween", 0.2f);
    }

    private void DeactiveTween()
    {
        keys[prevStep].SendMessage("Stop");
        keys[prevStep].SendMessage("ReverseWithTag", "Press");
    }

    private void NextKeyStart()
    {
        prevStep = sequenceStep;
        sequenceStep++;

        if (sequenceStep > sequenceLimit)
        {
            sequenceStep = 0;
        }

        PotaTween[] tweens = keys[sequenceStep].GetComponentsInChildren<PotaTween>();

        //Debug.Log(tweens.Length);

        /*int count = 0;

        while (count < tweens.Length)
        {
            tweens[count].Play();

            count++;
        }*/
        //keys[sequenceStep].transform.FindChild("frame-feedback").GetComponent<PotaTween>()
        //keys[sequenceStep].SendMessage("PlayWithTag", "Feedback");
    }

    void OnEnable()
    {
        textCombo.transform.position = comboPosition;
        StartSequence();
    }

    private void CreateSequence()
    {
        //TODO:Se o cara clicar em duas ao mesmo tempo não pode funcionar.
        keys = new List<GameObject>();
        sequence = new List<KeyCode>();

        #region Keyboard Sequences
        int currentDay = GameManager.Instance.currentDay;
        switch (currentDay)
        {
            case 1:
                sequence.Add(KeyCode.W);
                sequence.Add(KeyCode.A);
                sequence.Add(KeyCode.S);
                sequence.Add(KeyCode.D);
                sequence.Add(KeyCode.D);
                sequence.Add(KeyCode.S);
                sequence.Add(KeyCode.A);
                sequence.Add(KeyCode.W);
                break;

            case 2:
                sequence.Add(KeyCode.A);
                sequence.Add(KeyCode.A);
                sequence.Add(KeyCode.D);
                sequence.Add(KeyCode.D);
                sequence.Add(KeyCode.S);
                sequence.Add(KeyCode.D);
                sequence.Add(KeyCode.S);
                sequence.Add(KeyCode.A);
                break;

            case 3:
                sequence.Add(KeyCode.W);
                sequence.Add(KeyCode.D);
                sequence.Add(KeyCode.S);
                sequence.Add(KeyCode.A);
                sequence.Add(KeyCode.S);
                sequence.Add(KeyCode.D);
                sequence.Add(KeyCode.S);
                sequence.Add(KeyCode.A);
                break;

            case 4:
                sequence.Add(KeyCode.A);
                sequence.Add(KeyCode.S);
                sequence.Add(KeyCode.A);
                sequence.Add(KeyCode.D);
                sequence.Add(KeyCode.A);
                sequence.Add(KeyCode.S);
                sequence.Add(KeyCode.D);
                sequence.Add(KeyCode.W);
                break;

            case 5:
                sequence.Add(KeyCode.A);
                sequence.Add(KeyCode.T);
                sequence.Add(KeyCode.D);
                sequence.Add(KeyCode.W);
                sequence.Add(KeyCode.A);
                sequence.Add(KeyCode.T);
                sequence.Add(KeyCode.D);
                sequence.Add(KeyCode.W);
                break;

            case 6:
                sequence.Add(KeyCode.X);
                sequence.Add(KeyCode.A);
                sequence.Add(KeyCode.W);
                sequence.Add(KeyCode.D);
                sequence.Add(KeyCode.S);
                sequence.Add(KeyCode.A);
                sequence.Add(KeyCode.W);
                sequence.Add(KeyCode.S);
                break;
        }
        #endregion

        /* | AJUSTE PARA CRIAR MAIS DE UM | */
        sequenceStep = 1;

        for (int i = 0; i < sequence.Count; i++)
        {
            GameObject key = Instantiate(keyModel);
            Transform keyTransform = key.transform;
            TextMesh keyText = key.GetComponent<TextMesh>();

            keyText.text = sequence[i].ToString();
            key.transform.parent = keysContainer;
            keyTransform.localPosition = Vector3.right * (KEY_OFFSET * i);

            if (i > sequenceLimit)
            {
                key.SetActive(false);
            }

            keys.Add(key);
        }


        Vector2 keySize = keys[0].transform.Find("frame").GetComponent<SpriteRenderer>().bounds.size;
        keyBall.Setup(keys[0], keySize);
    }

    public void CentralizeSequence()
    {
        //keysContainer
        float posX = (keys[0].transform.localPosition.x - keys[sequenceLimit].transform.localPosition.x) * 0.5f;
        PotaTween positionTween = PotaTween.Create(keysContainer.gameObject);
        positionTween.SetPosition(TweenAxis.X, keysContainer.transform.localPosition.x, posX, true);
        positionTween.SetDuration(0.5f);
        positionTween.SetEaseEquation(Ease.Equation.OutBack);

        positionTween.Play();
    }

    public void OpenGame()
    {
        if (stopGameplay)
        {
            telephone = GameManager.Instance.GetInteractableByName("Telephone");

            if (telephone != null)
            {
                telephone.gameObject.GetComponent<TelephoneMenu>().CloseMenu();
            }

            SetComboLayer(gameObject.layer);

            keyboardIcon.SetActive(true);
            sequenceComboText.gameObject.SetActive(false);
            clickGameContainer.SetActive(true);
            ResetSequenceTween();
            GameManager.Instance.DrinkMode();
            distanceRot = 0;
            blur.enabled = true;
            startGame = true;
            blur.blurSize = 0.5f;

            textTimer.text = "";

            speedAnim = 2;
            combo = 0;

            textCombo.transform.localScale = Vector3.zero;

            liquidDrink.localPosition = Vector3.zero + (Vector3.up * 0.1f);

            DecreaseLiquid();

            gameContainerTween.Reverse();
            ownTransform.rotation = new Quaternion(0, 0, 0, 0);

            if (finalDrink)
            {
                timer = 61f;
                textTimer.text = "";
                //textTimer.text = "Final Trago"; 
            }
            else
            {
                closeDrinkGameButton.gameObject.SetActive(true);
            }

            CentralizeSequence();
        }
    }

    private void SetComboLayer(int currentLayer)
    {

        textCombo.gameObject.layer = currentLayer;
        Transform textTransform = textCombo.transform;

        int count = 0;

        while (count < textTransform.childCount)
        {
            textTransform.GetChild(count).gameObject.layer = currentLayer;
            count++;
        }
    }

    void Update()
    {
        if (!startGame && stopGameplay) return;

        if (!refillTime && keys.Count > 0)
        {
            int count = 0;

            PotaTween tween = keys[sequenceStep].GetComponent<PotaTween>();

            bool mouseButtonDown = Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1);

            if (mouseButtonDown) return;

            if ((Input.anyKeyDown && !Input.GetKeyDown(sequence[sequenceStep])))
            {
                clickGameContainer.SendMessage("Shake", 0.2f);
                Error(StringTable.GetText("WRONG_KEY_WARNING"));
            }

            if (Input.GetKeyDown(sequence[sequenceStep]))
            {
                tween.Stop();
                tween.PlayWithTag("Press");
            }

            if (Input.GetKeyUp(sequence[sequenceStep]))
            {
                tween.Stop();
                tween.ReverseWithTag("Press");
            }
        }

    }

    private void Error(string text)
    {

        sequenceComboText.gameObject.SetActive(true);
        sequenceComboText.text = text;
        sequenceComboText.SendMessage("PlayWithTag", "EndCombo");

        sequenceComboCount = 0;


        TakeTime();
    }

    private void UpdateSequenceCounter()
    {
        sequenceComboCount++;

        if ((combo * sequenceComboCount) > (maxTragoCombo * maxSequenceCombo))
        {
            maxTragoCombo = Mathf.Max(combo, maxTragoCombo);
            maxSequenceCombo = Mathf.Max(maxSequenceCombo, sequenceComboCount);
        }

        if (sequenceComboCount > 2)
        {
            if (!sequenceComboText.gameObject.activeInHierarchy)
            {
                sequenceComboText.gameObject.SetActive(true);
            }

            sequenceComboText.text = sequenceComboCount.ToString();
            sequenceComboText.SendMessage("PlayWithTag", "Pop");
        }

        if (keyboardIcon.activeInHierarchy)
        {
            keyboardIcon.SetActive(false);
        }
    }

    void FixedUpdate()
    {

        if (stopGameplay || pause) return;

        if (refillTime && !finalDrink) return;

        timer -= Time.fixedDeltaTime;

        timer = Mathf.Max(0, timer);

        if (timer > 0)
        {
            textTimer.text = ClockUtils.SecondTime(timer);

            if (timer < 2)
                textTimer.color = Color.red;

            return;
        }

        textTimer.text = ClockUtils.SecondTime(0);
        if (finalDrink)
        {
            LostCombo();
            sequenceComboText.text = "That's Enough!";
            sequenceComboText.SendMessage("PlayWithTag", "Pop");
            return;
        }

        sequenceComboText.text = StringTable.GetText("STOP_DRINKING");
        sequenceComboText.SendMessage("PlayWithTag", "Pop");
        closeDrinkGameButton.OncloseDrinkGameClicked();
    }

    private bool CanDoNext()
    {
        int prevStep = sequenceStep - 1;

        if (prevStep < 0)
        {
            prevStep = sequenceLimit;
        }

        PotaTween keyTween = keys[sequenceStep].GetComponentInChildren<PotaTween>();

        /*float tweenTime = keyTween.ElapsedTime;
        float duration = keyTween.Duration * 0.2f;

        float delay = keyTween.ElapsedDelay;
        float delayDur = keyTween.Delay * 0.2f;*/

        return !keyTween.IsPlaying;// tweenTime > 0;
    }

    public void LostCombo()
    {
        if (clickGameContainer.activeSelf)
        {
            stopGameplay = true;
            blur.enabled = false;

            if (drunkController) drunkController.StartDecrease();

            if (!finalDrink)
            {
                speedAnim = 2;
                Invoke("CloseDrinkingGame", 0.3f);

            }
            else
            {
                finalCombo = combo;
                StartCoroutine(FinalDrinkCloseGameClick());
            }

            if (combo > 0)
            {
                doubleGlassVision.SetTrigger("Reset");
                drunkAnimation = 0;
                doubleGlassVision.SetInteger("howDrunk", drunkAnimation);
                AnimateComboLost();

                if (jucaCanFallDrunk && combo >= minimumComboToJucaFall)
                {
                    jucaCanFallDrunk = false;
                    jucaAnimator.SetTrigger("Drunk");
                    GameManager.Instance.SendMessage("DrunkEnd");

                    if (GameManager.Instance.currentDay == 4)
                    {
                        AchievementController.Instance.AchievementComplete(11);
                        return;
                        //Invoke("PlayFallDrunkActions",5);
                    }
                }
            }
        }
    }

    private void PlayFallDrunkActions()
    {
        if (fallDrunkAction)
        {
            fallDrunkAction.DoAction();
        }
    }

    private void AnimateComboLost()
    {
        PotaTween cardAnimation1 = PotaTween.Create(textCombo.gameObject, 3);
        PotaTween cardAnimation2 = PotaTween.Create(textCombo.gameObject, 4);
        PotaTween cardAnimation3 = PotaTween.Create(textCombo.gameObject, 5);

        Transform comboTransform = textCombo.transform;

        cardAnimation1.Clear();
        cardAnimation2.Clear();

        Vector3 finalPos = new Vector3(-5.3f, 0.45f, 0);

        if (telephone != null && !(combo >= minimumComboToJucaFall))
        {
            finalPos = telephone.transform.position;
            finalPos.z -= 1;

            SetComboLayer(telephone.gameObject.layer);
            Invoke("UpdateTelephoneTragos", 1.4f);
        }

        cardAnimation1.SetPosition(comboTransform.position, finalPos);
        cardAnimation1.SetEaseEquation(Ease.Equation.OutSine);
        cardAnimation1.SetDelay(0.7f);

        cardAnimation2.SetScale(comboTransform.localScale, Vector3.one * 0.3f);
        cardAnimation2.SetCurve(comboCurve);
        cardAnimation2.SetDuration(0.8f);
        cardAnimation2.SetDelay(0.7f);

        cardAnimation3.SetAlpha(1f, 0.0f);
        cardAnimation3.SetDuration(0.2f);
        cardAnimation3.SetDelay(1.4f);
        cardAnimation3.SetEaseEquation(Ease.Equation.OutSine);
        cardAnimation3.Tag = "Alpha";

        cardAnimation1.SetDuration(0.8f);

        cardAnimation1.Play();
        cardAnimation2.Play();
        cardAnimation3.Play();

    }

    private void UpdateTelephoneTragos()
    {
        telephone.SendMessage("SetTragoCount", tragoCounter);
        combo = 0;
    }

    IEnumerator FinalDrinkCloseGameClick()
    {
        yield return new WaitForSeconds(1);
        gameContainerTween.Play();

        if (finalCombo > 2)
        {
            if (jucaAnimator)
            {
                jucaAnimator.SetTrigger("Drunk");
                jucaScene5.EndDialogue();
            }
            yield return new WaitForSeconds(6);
        }

        jucaScene5.CallEndScreen();
        //NightTimeController.NIGHT_COUNT = 6;
        GameManager.Instance.SendMessage("FinishNightCloseAll");
    }

    private void CloseDrinkingGame()
    {
        gameContainerTween.Play(DeativateContainer);

        Vector2 keySize = keys[0].transform.Find("frame").GetComponent<SpriteRenderer>().bounds.size;
        keyBall.Setup(keys[0], keySize);

        ResetSequence();
        GameManager.Instance.SendMessage("CloseDrinkingGame");
    }

    public void ChangeLiquidSprite(Sprite sprite)
    {
        SpriteRenderer renderer = liquidDrink.GetComponent<SpriteRenderer>();
        renderer.color = new Color(1, 1, 1, 0.8f);
        renderer.sprite = sprite;


    }

    private void ResetSequence()
    {
        sequenceLimit = 1;
        sequenceStep = 0;
        energyPercent = 0;
        sequenceComboCount = 0;

        int count = 0;

        while (count < keys.Count)
        {
            if (count > sequenceLimit)
            {
                keys[count].GetComponent<TextMesh>().color = keyBall.baseColor;

                keys[count].SetActive(false);
            }

            count++;
        }
    }

    private void DeativateContainer()
    {
        clickGameContainer.SetActive(false);
    }

    void DecreaseLiquid()
    {
        float posY = Mathf.Max(Mathf.Abs(liquidDrink.localPosition.y), liquidDrink.GetComponent<SpriteRenderer>().bounds.size.y * (0.15f + energyPercent));
        float maxRot = -10f;
        float rotZ = Ease.Linear(energyPercent, 0, maxRot, 1);

        if (liquidTween)
        {
            liquidTween.Clear();
            liquidTween.SetPosition(TweenAxis.Y, liquidDrink.localPosition.y, posY * -1, true);
            liquidTween.SetDuration(0.15f);
            //liquidTween.SetRotation(TweenAxis.Z, transform.rotation.eulerAngles.z, rotZ);
            liquidTween.SetEaseEquation(Ease.Equation.OutBack);

            liquidTween.Play();
        }


        liquidDrink.localPosition = (Vector3.down * (posY));
    }

    IEnumerator FadeOutofTheScreen()
    {
        yield return new WaitForSeconds(1);
        rotTween.SetScale(Vector3.one, Vector3.zero);
        rotTween.SetAlpha(1, 0);
    }

    private void TakeTime()
    {
        if (timer > 0)
        {
            popTimer.Play();
            timer -= 1.0f;
        }
    }

    private void Drink()
    {
        if (stopGameplay && startGame)
        {
            stopGameplay = false;
            startGame = false;

            if (!finalDrink)
            {
                timer = TIME * speedAnim;
            }
        }
        else if (stopGameplay && !startGame)
        {
            return;
        }

        if (refillTime) return;

        FMOD_StudioSystem.instance.PlayOneShot("event:/Copo/clicandoCopo", transform.position);

        /*liquidBar.SendMessage(ClickMessages.CLICK_IN, info);*/
        energyPercent += 0.1f;
        DecreaseLiquid();

        distanceRot = Ease.Linear(energyPercent, 0, rotLimit.y, 1);
        //energyPercent = 

        if (energyPercent > 0.9f)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            //liquidBar.Reset();

            rotTween.Clear();

            rotTween.SetRotation(TweenAxis.Z, distanceRot, 0);
            rotTween.SetDuration(0.5f * speedAnim);
            rotTween.SetAlpha(0.0F, 1F);
            rotTween.SetEaseEquation(Ease.Equation.OutBack);

            distanceRot = 0;

            doubleGlassVision.SetBool("CanStartAnimation", false);
            rotTween.Play(TakeGlass);
            NextSequenceLevel();

            energyPercent = 0;

            refillTime = true;

            return;
        }

        /* * (Mathf.Round(energyPercent * 100) / 100);*/

        rotTween.SetRotation(TweenAxis.Z, transform.eulerAngles.z, distanceRot);
        rotTween.SetDuration(0.2f);
        rotTween.SetEaseEquation(Ease.Equation.OutBack);
        rotTween.Play();
    }

    private void TakeGlass()
    {
        takeTween.Clear();
        takeTween.SetPosition(TweenAxis.X, ownTransform.parent.position.x, -12);
        takeTween.SetDuration(0.5f * speedAnim);
        takeTween.SetEaseEquation(Ease.Equation.InBack);
        FMOD_StudioSystem.instance.PlayOneShot("event:/Copo/copoarrastando", transform.position);

        SequenceComboAnimation();

        if (!openCombo.IsPlaying)
        {
            combo++;
            GameManager.HowManyDrinks++;

            if (drunkController)
            {
                drunkController.AddBlur();
            }

            tragoCounter++;
            todaysTrago.text = tragoCounter.ToString();

            if (uiTragoCounter)
            {
                uiTragoCounter.text = "" + tragoCounter;
            }

            if (combo.ToString().Length == 1)
            {
                textCombo.text = "0" + combo;
            }
            else
            {
                textCombo.text = "" + combo;
            }

            if (combo == 2)
            {
                FMOD_StudioSystem.instance.PlayOneShot("event:/Copo/combo", transform.position);
                openCombo.Play();
                doubleGlassVision.SetTrigger("StartToSeeDoubler");
            }
            else if (combo > 2)
            {
                FMOD_StudioSystem.instance.PlayOneShot("event:/Copo/combo", transform.position);
                popCombo.Play();
                drunkAnimation++;
                doubleGlassVision.SetInteger("howDrunk", drunkAnimation);
            }

            if (finalDrink)
            {
                if (combo == 3)
                    jucaScene5.ShowImDrunkDialogue();
                else
                    jucaScene5.ShowRandomDrunkDialogue();
            }
        }

        doubleGlassVision.enabled = false;
        takeTween.Play();

        Invoke("FillGlass", 0.6f * speedAnim);
    }

    private void SequenceComboAnimation()
    {
        sequenceComboText.text = sequenceComboCount + "x" + (combo + 1) + " = " + (sequenceComboCount * (combo + 1));
        sequenceComboText.SendMessage("PlayWithTag", "Pop");
        //sequenceComboText.GetComponent<PotaTween>().Reverse();
    }

    private void FillGlass()
    {
        FMOD_StudioSystem.instance.PlayOneShot("event:/Copo/copoInvertido", transform.position);
        liquidDrink.localPosition = Vector3.zero;
        liquidDrink.rotation = Quaternion.identity;
        DecreaseLiquid();
        takeTween.Reverse();

        Invoke("Restart", 0.5f * speedAnim);

        Vector2 keySize = keys[0].transform.Find("frame").GetComponent<SpriteRenderer>().bounds.size;
        keyBall.Setup(keys[0], keySize);

        keyContainerOut.Stop();
        keyContainerOut.Reverse();
        sequenceStep = 0;


        sequenceComboCount = combo * sequenceComboCount;
        sequenceComboText.text = "" + sequenceComboCount;
        sequenceComboText.SendMessage("PlayWithTag", "Pop");
        textTimer.color = Color.black;
    }

    private void Restart()
    {
        doubleGlassVision.SetBool("CanStartAnimation", true);
        GetComponent<BoxCollider2D>().enabled = true;
        refillTime = false;
        speedAnim = Mathf.Max(speedAnim - 0.2f, speedMin);
        blur.blurSize += 0.5f / speedAnim;
        doubleGlassVision.enabled = true;

        if (!finalDrink)
            timer = TIME * speedAnim;
    }

    public void PauseOn()
    {
        pause = true;
    }

    public void PauseOff()
    {
        pause = false;
    }

}
