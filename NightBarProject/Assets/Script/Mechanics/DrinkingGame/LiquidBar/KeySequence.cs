﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KeySequence {

    private List<KeyCode> keys = new List<KeyCode>();
    private int currentKeyIndex = 0;

    public void AddKey(KeyCode key)
    {
        keys.Add(key);
    }

    public KeyCode GetCurrentKey()
    {
        if (keys.Count > 0)
        {
            return keys[currentKeyIndex];
        }

        return KeyCode.Alpha0;
    }


    public void NextKey()
    {
        currentKeyIndex++;

        if (currentKeyIndex >= keys.Count)
        {
            currentKeyIndex = 0;
        }
    }
}
