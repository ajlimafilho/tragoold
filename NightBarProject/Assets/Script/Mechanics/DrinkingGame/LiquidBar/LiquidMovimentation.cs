﻿using UnityEngine;
using System.Collections;

public class LiquidMovimentation : MonoBehaviour {

    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = Vector3.Lerp(new Vector3(-5, transform.position.y,0), new Vector3(5, transform.position.y,0), Mathf.PingPong(Time.time, 1.0f));
    }
}
