﻿using UnityEngine;
using System.Collections;

public class KeyTimingBall : MonoBehaviour {

    public Color baseColor = Color.white;
    public GinGlass ginGlass;
    
    private Transform ownTransform;
    private TextMesh currentKeyMesh;

    private Vector2 jumpSize;
    private Vector3 startPosition;
    private GameObject startKey;
    private Color ballColor;
    private PotaTween tween;

    private bool kickOn = false;
    private bool jumpNext = false;

    private float step;

    private bool doPoing = false;
    private float stepMultiplier = 5f;
    private float poingHeight = 0.5f;

    private bool tryKick = false;
    private SpriteRenderer sprite;
    private SpriteRenderer frameSprite;

    void Awake () 
    {
        sprite = GetComponent<SpriteRenderer>();
        ballColor = sprite.color;
    }
    
    void FixedUpdate () 
    {
        if (doPoing)
        {
            Vector3 newPosition = ownTransform.localPosition;

            float sin = (Mathf.Sin(step));
            float cos = (Mathf.Cos(step));
            newPosition.y = startPosition.y + (poingHeight * Mathf.Abs(sin));

            Color newColor = Color.Lerp(ballColor, baseColor, Mathf.Abs(sin));
            
            currentKeyMesh.color = newColor;
            frameSprite.color = newColor;

            newColor.a = 0.3f + (1 - Mathf.Abs(sin));//Color.Lerp(ballColor, baseColor, Mathf.Abs(sin));
            sprite.color = newColor;

            //poingHeight = Mathf.Max(poingHeight, poingHeight - Time.deltaTime);

            if (jumpNext)
            {
                newPosition.x = currentKeyMesh.transform.localPosition.x;

                float kickStep = step / 4;
                newPosition = Vector3.Lerp(ownTransform.localPosition, newPosition, Mathf.Abs(sin) / 1.5f);
                newPosition.y = startPosition.y + (poingHeight * Mathf.Abs(sin));

                if (kickStep > 0.5f)
                {
                    kickOn = false;
                }

                if (kickStep > 1f)
                {
                    jumpNext = false;
                }
            }

            bool insideArea = (sin < 0.35 && sin > -0.35f);

            if (/*insideArea &&*/ !kickOn)
            {
                TryKick(insideArea);
            }
            
            newPosition.z = ownTransform.localPosition.z;
            step += stepMultiplier * Time.deltaTime;
            ownTransform.localPosition = newPosition;
        }
    }

    private void TryKick(bool insideArea)
    {
        
        if ((tween.IsPlaying && !tween.IsReversing))
        {
            tryKick = true;
            if (insideArea)
            {
                step = 0;
                kickOn = true;
                jumpNext = true;
                ginGlass.SendMessage(DrinkMessages.NEXT_SEQUENCE_STEP);
                //currentKeyMesh = ginGlass.CurrentKey.GetComponent<TextMesh>();
                tryKick = false;
            }
            return;
        }

        if(tryKick && !insideArea)
        {
            tryKick = false;
            ginGlass.SendMessage("Error", StringTable.GetText("MISS_SEQUENCE_WARNING"));
        }

    }

    public void Setup(GameObject key, Vector2 size)
    {
        ownTransform = transform;
        jumpSize = size;
        startKey = key;

        currentKeyMesh = key.GetComponent<TextMesh>();
        frameSprite = key.transform.Find("frame").GetComponent<SpriteRenderer>();
        tween = key.GetComponent<PotaTween>();

        Vector3 setupPosition = key.transform.localPosition;
        setupPosition.y += size.y * .65f;
        //setupPosition.x -= GinGlass.KEY_OFFSET * .5f;

        ownTransform.localPosition = setupPosition;
        startPosition = setupPosition;


        doPoing = true;
    }

    public void Reset()
    {
        Setup(startKey, jumpSize);
    }
}
