﻿using UnityEngine;
using System.Collections;

public class LiquidBar : MonoBehaviour, IPausable {

    public float EnergyPercent
    {
        get
        {
            return 1 + (energy.localPosition.x / distance);
        }
    }

    public Transform energy;
    public ParticleSystem particle;

    public float clickForce;
    public float force;
    public Vector2 forceExponential;

    private Vector2 energyLimits = new Vector2();
    private float distance = 0;
    private bool pause = false;

    void OnEnable () {
        if (distance == 0)
        {
            energyLimits.x = energy.localPosition.x - (energy.GetComponent<SpriteRenderer>().bounds.size.x / transform.localScale.x);
            energyLimits.y = energy.localPosition.x;

            distance = Mathf.Abs(energyLimits.x - energyLimits.y);

            Vector3 newPos = energy.transform.localPosition;
            newPos.x = energyLimits.x;

            energy.transform.localPosition = newPos;
        }
    }
    
    void FixedUpdate () {

        if (pause) return;

        float currentExponential = forceExponential.y + ((forceExponential.y * Mathf.Abs(energy.localPosition.x)) / distance);
        currentExponential = Mathf.Clamp(currentExponential, forceExponential.x, forceExponential.y);

        if (distance > Mathf.Abs(energy.localPosition.x))
        {
            Vector3 newPos = energy.transform.localPosition;
            newPos.x -= force * currentExponential;

            energy.transform.localPosition = newPos;
        }
    }

    private void OnClickIn(ClickInfo info)
    {
        

        Vector3 newPos = energy.transform.localPosition;
        newPos.x += clickForce;

        if (0 >= newPos.x)
        {
            energy.transform.localPosition = newPos;
            return;
        }
    }

    internal void Reset()
    {
        particle.gameObject.SetActive(false);
        particle.gameObject.SetActive(true);
        Vector3 newPos = energy.transform.localPosition;
        newPos.x = energyLimits.x;
        energy.transform.localPosition = newPos;
    }

    public void PauseOn()
    {
        pause = true;
    }

    public void PauseOff()
    {
        pause = false;
    }
}
