﻿using UnityEngine;
using System.Collections;

public class GlassMovimentation : MonoBehaviour {

    public Transform position1;
    public Transform position2;

    void Start ()
    {
    
    }
    
    // Update is called once per frame
    void Update ()
    {
        transform.position = Vector3.Lerp(position1.position, position2.position, Mathf.PingPong(Time.time, 1.0f));
    }
}
