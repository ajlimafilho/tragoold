﻿using UnityEngine;
using System.Collections;

public class ClickDispatcher : MonoBehaviour, IPausable {

    private int count = 0;
    private bool mouseOver = false;
    private bool pause = false;

    public GameAction action;
    public GameObject stopButton;
    public bool sendMessages = true;

    void Update()
    {
        if (pause) return;

        if (Input.GetMouseButtonDown(0))
        {
            if (!mouseOver && sendMessages)
            {
                Vector3 inputPosition = Input.mousePosition;
                Vector2 ray = Camera.main.ScreenToWorldPoint(inputPosition);

                ClickInfo info = new ClickInfo(ray, count);
                SendMessage(ClickMessages.CLICK_OUT, info);
            }
        }

    }

    void OnMouseEnter()
    {
        mouseOver = true;
    }

    void OnMouseExit()
    {
        mouseOver = false;
    }

    void OnMouseDown()
    {
        if (pause) return;

        Vector3 inputPosition = Input.mousePosition;

        //new Vector3(Screen.width - inputPosition.x, Screen.height - inputPosition.y, Camera.main.transform.position.z - 2f)

        if (action)
        {
            action.DoAction();
        }

        if (sendMessages)
        {
            Vector2 ray = Camera.main.ScreenToWorldPoint(inputPosition);
            ClickInfo info = new ClickInfo(ray, count);
            
            SendMessage(ClickMessages.CLICK_IN, info);
        }
        count++;
    }

    public void PauseOn()
    {
        pause = true;
    }

    public void PauseOff()
    {
        pause = false;
    }
}
