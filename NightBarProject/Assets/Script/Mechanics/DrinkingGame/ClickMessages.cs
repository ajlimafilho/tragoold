﻿using UnityEngine;
using System.Collections;

public class ClickMessages {
    public const string CLICK_IN = "OnClickIn";
    public const string CLICK_OUT = "OnClickOut";
}
