﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CloseDrinkGame : MonoBehaviour {

    public GinGlass ginGlass;
    private Button closeDrinkGameButton;
    
    private PotaTween buttonTween;

    void Awake()
    {
        closeDrinkGameButton = GetComponent<Button>();
        closeDrinkGameButton.onClick.AddListener(OncloseDrinkGameClicked);

        buttonTween = PotaTween.Create(gameObject);
        buttonTween.SetScale(Vector3.zero, Vector3.one);
        buttonTween.SetDuration(0.4F);
        buttonTween.SetEaseEquation(Ease.Equation.OutSine);
        buttonTween.SetRotation(TweenAxis.Z, 60, 0);
    }

    void OnEnable()
    {
        buttonTween.Play();
    }
    
    public void OncloseDrinkGameClicked()
    {
        ginGlass.LostCombo();
        StartCoroutine(DisableButton());
    }

    IEnumerator DisableButton()
    {
        yield return new WaitForSeconds(.3f);
        buttonTween.Reverse();
        yield return new WaitForSeconds(.4f);
        gameObject.SetActive(false);
    }
}
