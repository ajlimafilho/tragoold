﻿using UnityEngine;
using System.Collections;

public class ClickInfo {

    public Vector2 MousePos
    {
        get
        {
            return mousePos;
        }
    }

    public int ClickCount
    {
        get
        {
            return count;
        }
    }

    private Vector2 mousePos;
    private int count;

    public ClickInfo(Vector2 mousePos, int count)
    {
        this.count = count;
        this.mousePos = mousePos;
    }

    
}
