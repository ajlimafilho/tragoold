﻿using UnityEngine;
using System.Collections;

public class FocusData {

    public Vector3 position;
    public Vector2 focusBounds;

    public Ease.Equation ease = Ease.Equation.Linear;
    public float duration = 0.3f;


    public static FocusData BasicFocus(Vector3 position)
    {
        FocusData focus = new FocusData();
        focus.focusBounds = new Vector2(4, 5);
        focus.ease = Ease.Equation.OutSine;
        focus.duration = 0.3f;
        focus.position = position;

        return focus;
    }

    public static FocusData HitFocus(Vector3 position)
    {
        FocusData focus = new FocusData();
        focus.focusBounds = new Vector2(4.5f, 5);
        focus.ease = Ease.Equation.OutCirc;
        focus.duration = 0.3f;
        focus.position = position;

        return focus;
    }

    public static FocusData RewardFocus(Vector3 position)
    {
        FocusData focus = new FocusData();
        focus.focusBounds = new Vector2(3.5f, 5);
        focus.ease = Ease.Equation.OutCirc;
        focus.duration = 0.3f;
        focus.position = position;

        return focus;
    }
}
