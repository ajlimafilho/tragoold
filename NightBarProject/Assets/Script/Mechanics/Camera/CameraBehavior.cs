﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class CameraBehavior : MonoBehaviour, IPausable {

    private const float BASE_FOCUS = 5;
    private const float CLOSE_FOCUS = 3.8F;

    public GameObject target;
    public float followSpeed = 4;

    //TODO: Usar Property para isso.
    [HideInInspector]
    public bool isInsideBound;

    public BoxCollider2D boxBounds;

    private Transform ownTransform;
    private Transform targetTransform;
    private Bounds targetBounds;

    private Camera camera;
    private Bounds cameraBounds;
    private Bounds levelBounds;

    private bool canFollowTarget = true;
    private Vector3 levelSizeDifference;

    private BlurOptimized blur;
    private float focus;
    private float timeFocus = 0;
    private bool refreshFocus = false;
    private Vector3 saveCameraPos;

    private FocusData focusData;
    private bool pause;

    void Awake()
    {
        if (boxBounds)
        {
            levelBounds = boxBounds.bounds;
        }

        ownTransform = transform;
        focusData = FocusData.BasicFocus(ownTransform.position);

        camera = GetComponent<Camera>();
        cameraBounds = OrthographicBounds();

        blur = GetComponent<BlurOptimized>();

        if (target)
        {
            targetTransform = target.GetComponent<Transform>();
            targetBounds = targetTransform.GetComponent<BoxCollider2D>().bounds;
        }

        refreshFocus = true;
    }

    void Start ()
    {
        levelSizeDifference = ((levelBounds.size - cameraBounds.size)) * 0.5f;
    }
    
    void FixedUpdate () 
    {

        if (pause) return;

        if (!isInsideBound)
        {
            Vector3 distance =  ownTransform.position - levelBounds.center;
            isInsideBound = (Mathf.Abs(distance.x) <= levelSizeDifference.x) && (Mathf.Abs(distance.y) <= levelSizeDifference.y);
        }

       
        SetFocus();
        MoveCameraInBounds();

        
    }

    private void SetFocus()
    {
        timeFocus += Time.fixedDeltaTime;

        if(canFollowTarget)
        {
            timeFocus -= Time.fixedDeltaTime * 2;
        } 

        System.Reflection.MethodInfo easeMethod = System.Type.GetType("Ease").GetMethod(focusData.ease.ToString());
        timeFocus = Mathf.Clamp(timeFocus, 0f, focusData.duration);

        focus = (float)easeMethod.Invoke(null, new object[] { timeFocus,  focusData.focusBounds.y, focusData.focusBounds.x - focusData.focusBounds.y, focusData.duration });
        
        camera.orthographicSize = focus;

        cameraBounds = OrthographicBounds();
        levelSizeDifference = ((levelBounds.size - (cameraBounds.size))) * 0.5f;
    }

    private void MoveCameraInBounds()
    {
        if (target == null && canFollowTarget) return;

        Vector3 distance = focusData.position - levelBounds.center;
        if (canFollowTarget) distance = targetTransform.position - levelBounds.center;

        float valueX = (distance.x * levelSizeDifference.x) / ((levelBounds.size.x - (targetBounds.size.x)) * .5f);
        float valueY = (distance.y * levelSizeDifference.y) / ((levelBounds.size.y - (targetBounds.size.y)) * .5f);

        Vector3 newPosition = levelBounds.center;
        newPosition.x += valueX;
        newPosition.y += valueY;
        newPosition.z = -10;

        distance = newPosition - ownTransform.position;

        ownTransform.position += distance * ((followSpeed * Time.fixedDeltaTime));
    }

    public void SetLevelBounds(Bounds bounds)
    {
        levelBounds = bounds;
        isInsideBound = false;
        levelSizeDifference = ((levelBounds.size - cameraBounds.size)) * 0.5f;
    }

    private Bounds OrthographicBounds()
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = camera.orthographicSize * 2;
        //Debug.Log(cameraHeight);
        
        Bounds bounds = new Bounds(
            camera.transform.position,
            new Vector3(cameraHeight * screenAspect, cameraHeight, 0));

        return bounds;
    }

    public void ActiveBlur()
    {
        blur.enabled = true;
    }

    public void DeactiveBlur()
    {
        blur.enabled = false;
    }

    public void SetTarget(GameObject newTarget)
    {
        target = newTarget;

        targetTransform = target.GetComponent<Transform>();
        targetBounds = targetTransform.GetComponent<BoxCollider2D>().bounds;
    }

    private void FocusOn(FocusData focusData)
    {
        this.focusData = focusData;
        canFollowTarget = false;
        saveCameraPos = ownTransform.position;
        refreshFocus = true;
        //timeFocus = 0;
    }

    public void SetSpeed(float value)
    {
        followSpeed = value;
    }

    private void FocusOff()
    {
        canFollowTarget = true;
    }

    public void PauseOn()
    {
        pause = true;
    }

    public void PauseOff()
    {
        pause = false;
    }
}
