﻿using UnityEngine;
using System.Collections;

public class CameraMessages{
    public const string FOCUS_ON = "FocusOn";
    public const string FOCUS_OFF = "FocusOff";
}
