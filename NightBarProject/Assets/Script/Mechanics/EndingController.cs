﻿using UnityEngine;
using System.Collections;

public class EndingController : MonoBehaviour {

    public static string COMPLETED_ENDS = "JUCA_DIE,CARLOS_ON_JAIL,SAVE_JOANA,FALL_DRUNK,SECRET_SHELF";
    public static string CURRENT_ENDING = "JUCA_DIE";

    public enum Endings
    {
        JUCA_DIE,
        CARLOS_ON_JAIL,
        SAVE_JOANA,
        FALL_DRUNK,
        SECRET_SHELF
    }

    public TextMesh EndingCounterDisplay;

	void Start () 
	{
	
	}

	public void FinishMoneyEnding()
	{
		AddKey (Endings.SECRET_SHELF.ToString());
	}

	public void FinishCarlosEnding()
	{
		AddKey (Endings.CARLOS_ON_JAIL.ToString());

	}

	public void FinishJucaEnding()
	{
        AddKey(Endings.JUCA_DIE.ToString());

	}

	public void FinishJoanaEnding()
	{
		AddKey (Endings.SAVE_JOANA.ToString());

	}

	public void FinishDrunkEnding()
	{
		AddKey (Endings.FALL_DRUNK.ToString());

	}

	public void DisplayEndingCounter(TextMesh text)
	{
	
	}

	private void AddKey(string key){

		if (!COMPLETED_ENDS.Contains (key)) {
			COMPLETED_ENDS += key + ",";
		}
	}
}
