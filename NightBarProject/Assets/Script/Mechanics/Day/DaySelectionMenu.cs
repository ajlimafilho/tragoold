﻿using UnityEngine;
using System.Collections;

public class DaySelectionMenu : MonoBehaviour {

    
    public LoadingBar loadingBar;

    void Start () 
    {
        StartCoroutine(StartAfter());
    }

    private IEnumerator StartAfter()
    {
        yield return new WaitForEndOfFrame();
        
        DayButton[] days = GetComponentsInChildren<DayButton>();

        int count = 0;

        //Trocar Lenght por valor salvo;
        while (count < days.Length)
        {
            days[count].Unlock();
            count++;
        }
    }
    
    void Update () 
    {
        
    }

    internal void OpenEpisode(int number)
    {
        gameObject.SetActive(false);
        loadingBar.gameObject.SetActive(true);
        loadingBar.StartLoading(number);

    }
}
