﻿using UnityEngine;
using System.Collections;

public class DayButton : MonoBehaviour {

    public Color activeOrangeColor;
    public TextMesh textDescription;
    public int sceneIndex;

    private bool unlockClick = false;
    private TextHover textHover;
    private TextMesh mesh;
    private DaySelectionMenu menu;

    void Awake () 
    {
        textHover = GetComponent<TextHover>();
        mesh = GetComponent<TextMesh>();
        menu = GetComponentInParent<DaySelectionMenu>();

    }

    void OnMouseDown()
    {
        if (unlockClick)
        {
            if (menu)
            {
                menu.OpenEpisode(sceneIndex);
            }
        }
    }

    public void Unlock()
    {
        if (textHover)
        {
            textHover.ChangeColor(Color.white, activeOrangeColor);
            mesh.color = Color.white;

            textDescription.name = "CLICK_TO_START";
            textDescription.fontSize = 80;
        }

        unlockClick = true;
    }
}
