﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class MedalBoard : MonoBehaviour {

    

    #region Singleton
    private static MedalBoard instance;

    public static MedalBoard Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<MedalBoard>();

            return instance;
        }
    }
    #endregion

    public List<GameObject> medals;
    private GameObject conquestMedal;

	void Start ()
    {
        int index = 0;

        string[] completedEndings = EndingController.COMPLETED_ENDS.Split(","[0]);
        
        while (index < medals.Count)
        {
            string drinkMedalName = medals[index].name;
            bool haveSave = EndingController.COMPLETED_ENDS.Contains(drinkMedalName);

            string animationMessage = "Reverse";

            if (haveSave)
            {
                animationMessage = "Play";
                medals[index].SendMessage("PlayFloat");

                if (!PlayerPrefs.GetString("ENDINGS").Contains(drinkMedalName))
                {
                    PotaTween tween = medals[index].GetComponent<PotaTween>();
                    tween.SetDelay(2f);
                    medals[index].transform.Find("NEW_ENDING").gameObject.SetActive(true);
                }
            }

            medals[index].GetComponent<BoxCollider2D>().enabled = haveSave;
            medals[index].SendMessage(animationMessage);
            
            index++;
        }
    }
	
	void Update ()
    {
	    
	}

    public static void SetupMedals()
    {
        /*JUCA_DIE_ENDING = 
        SECRET_SHELF_ENDING = PlayerPrefsX.GetBool("SECRET_SHELF_ENDING");
        FALL_DRUNK_ENDING = PlayerPrefsX.GetBool("FALL_DRUNK_ENDING");
        CARLOS_ON_JAIL_ENDING = PlayerPrefsX.GetBool("CARLOS_ON_JAIL_ENDING");
        SAVE_JOANA_ENDING = PlayerPrefsX.GetBool("SAVE_JOANA_ENDING");*/
    }
}
