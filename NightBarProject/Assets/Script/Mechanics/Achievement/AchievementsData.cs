﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;


[Serializable]
public class AchievementsData 
{
    const string PlayerPrefsKey = "AchievementsData";

    [JsonProperty("Achievements")]
    public List<int> Achievements = new List<int>();

    public static void SaveGameData(AchievementsData data)
    {
        string gameDataRawJson = JsonConvert.SerializeObject(data);

        Debug.LogWarningFormat("[Achievement] Saving Json: {0}", gameDataRawJson);

        PlayerPrefs.SetString(PlayerPrefsKey, gameDataRawJson);
        PlayerPrefs.Save();
    }
    public static AchievementsData LoadGameData()
    {
        if (!PlayerPrefs.HasKey(PlayerPrefsKey))
        {
            Debug.Log("No AchievementsData detected, creating new AchievementsData..");
            return new AchievementsData();
        }

        string gameDataRawJson = PlayerPrefs.GetString(PlayerPrefsKey, string.Empty);

        Debug.LogFormat("Trying to deserialize Achievement Data: {0}", gameDataRawJson);

        AchievementsData achievementsData = JsonConvert.DeserializeObject<AchievementsData>(gameDataRawJson);

        if (achievementsData == null)
        {
            Debug.Log("Invalid Achievements Data. Creating a new one.");
            achievementsData = new AchievementsData();
        }

        Debug.LogFormat("Reserialized Achievement Data: {0}", JsonConvert.SerializeObject(achievementsData));
        return achievementsData;
    }

    public static void ResetData()
    {
        PlayerPrefs.DeleteAll();
    }

}
