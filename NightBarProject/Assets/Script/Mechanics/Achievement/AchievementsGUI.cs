﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AchievementsGUI : MonoBehaviour
{
    [Range(1, 4)]
    public int AchievementDay;

    public bool WasWon;

    public Sprite badgeSprite;
    public PotaTween AchievementTween;
    public Image AchievementsFillImage;

    public Color AchievementWonTextColor;
   
    public Text TopBorderText;
    public Text BottomBorderText;

    private Color _achievementNotWonYetTextColor = Color.gray;

    void OnEnable()
    {
        AchievementsFillImage.fillAmount = WasWon ? 1 : 0;
        TopBorderText.color = WasWon ? AchievementWonTextColor : _achievementNotWonYetTextColor;
        BottomBorderText.color = WasWon ? AchievementWonTextColor : _achievementNotWonYetTextColor;
    }

    public int GetDay()
    {
        return AchievementDay;
    }

    public void EnableGameObject(bool active)
    {
        gameObject.SetActive(active); 
        if(!active)
            transform.localScale = new Vector3(1, 0, 1);
    }

    public void PlayTween()
    {
        AchievementTween.Play();
    }
}