﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

public class AchievementController : SingletonMonoBehaviour<AchievementController>
{
    public GameObject title;
    public AchievementsGUI[] AchievementsGUIItens;
    public AchievementsGUI AchievementWonGUI;
    private List<AchievementsGUI> _currentDayAchievements = new List<AchievementsGUI>();
    public GridLayoutGroup AchievementsGrid;
    public PotaTween AchievementWindowImage;
    public float TimeBetweenEachOneAchievementTween = .5f;

    void Start()
    {
        //AchievementsData.ResetData();
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        LoadGameData();
        yield return null;
        yield return null;
        
        for (int i = 0; i < AchievementsGUIItens.Length; i++)
        {
            if (AchievementsGUIItens[i].GetDay() == GameManager.Instance.currentDay)
            {
                _currentDayAchievements.Add(AchievementsGUIItens[i]);
                if (_gameData.Achievements.Contains(i))
                {
                    AchievementsGUIItens[i].WasWon = true;
                }
            }
        }
    }

    public void AchievementComplete(int achievementIndex)
    {
        if (_gameData.Achievements.Contains(achievementIndex))
            return;

        _gameData.Achievements.Add(achievementIndex);
        Debug.LogWarning(achievementIndex);
        AchievementWon(achievementIndex);
        SaveData();
    }

    public bool IsAchievementComplete(int achievementIndex)
    {
        return _gameData.Achievements.Contains(achievementIndex);
    }

    public void ShowTheCurrentDayAchievementsOnPauseGame()
    {
        StartCoroutine(PlayOnlyTheCurrentDayAchievementsTweenCoroutine());
    }

    public void ShowAllAchievementsOnAchievementWindow()
    {
        StartCoroutine(PlayAllAchievementsTweenCoroutine());
    }

    public void DisableAchievementPanel()
    {

        title.SetActive(false);
        for (int i = 0; i < AchievementsGUIItens.Length; i++)
        {
            AchievementsGUIItens[i].EnableGameObject(false);
        }
        
        AchievementsGrid.gameObject.SetActive(false);

        if (_areAllOpen)
            AchievementWindowImage.Reverse();

        _areAllOpen = false;
    }

    IEnumerator PlayOnlyTheCurrentDayAchievementsTweenCoroutine()
    {
        title.SetActive(true);
        AchievementsGrid.gameObject.SetActive(true);
        AchievementsGrid.constraintCount = 1;
        AchievementsGrid.cellSize = new Vector2(200f, 225f);

        if (Camera.main.pixelWidth > 1280)
            AchievementsGrid.cellSize = new Vector2(300f, 325f);



        for (int i = 0; i < _currentDayAchievements.Count; i++)
        {
            _currentDayAchievements[i].EnableGameObject(true); // first we enable all to hold position on grid 
        }

        for (int i = 0; i < _currentDayAchievements.Count; i++)
        {
            _currentDayAchievements[i].PlayTween();
            yield return new WaitForSeconds(TimeBetweenEachOneAchievementTween);
        }
    }

    private bool _areAllOpen;
    IEnumerator PlayAllAchievementsTweenCoroutine()
    {
        AchievementsGrid.gameObject.SetActive(true);
        AchievementsGrid.constraintCount = 2;
        AchievementsGrid.cellSize = new Vector2(250f, 275f);
        AchievementWindowImage.Play();
        _areAllOpen = true;

        for (int i = 0; i < _currentDayAchievements.Count; i++)
        {
            _currentDayAchievements[i].EnableGameObject(false);
        }

        for (int i = 0; i < AchievementsGUIItens.Length; i++)
        {
            AchievementsGUIItens[i].EnableGameObject(true);
        }

        for (int i = 0; i < AchievementsGUIItens.Length; i++)
        {
            AchievementsGUIItens[i].PlayTween();
            yield return new WaitForSeconds(TimeBetweenEachOneAchievementTween);
        }
    }

    int currentAchievementWon;

    void AchievementWon(int achievementIndex)
    {
        if (!AchievementsGUIItens[achievementIndex].WasWon)
        {
            AchievementWonGUI.gameObject.SetActive(true);

            AchievementWonGUI.SendMessage("Play");
            AchievementWonGUI.TopBorderText.text = StringTable.GetText(AchievementsGUIItens[achievementIndex].TopBorderText.name);
            AchievementWonGUI.AchievementsFillImage.sprite = AchievementsGUIItens[achievementIndex].badgeSprite;
            AchievementWonGUI.BottomBorderText.text = StringTable.GetText(AchievementsGUIItens[achievementIndex].BottomBorderText.name);
            AchievementsGUIItens[achievementIndex].WasWon = true;

            StartCoroutine(WaitToHideAchievementWonCoroutine());
        }
    }

    IEnumerator WaitToHideAchievementWonCoroutine()
    {
        yield return new WaitForSeconds(5);
        AchievementWonGUI.SendMessage("Reverse");
        Invoke("DeactivateAchievement", 1.5f);
    }

    private void DeactivateAchievement()
    {
        AchievementWonGUI.gameObject.SetActive(false);

    }

    private AchievementsData _gameData;

    private void LoadGameData()
    {
        _gameData = AchievementsData.LoadGameData();
    }

    void SaveData()
    {
        AchievementsData.SaveGameData(_gameData);
    }

    void Update()
    {
#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.F1))
            AchievementsData.ResetData();
#endif
    }
} 