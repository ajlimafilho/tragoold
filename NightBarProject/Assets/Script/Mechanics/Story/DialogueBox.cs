﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogueBox : MonoBehaviour, IPausable {

    public TextMesh lineText;

    public bool hasOptions;
    public bool wasRead;
    public List<Option> options;

    public List<InteractableChangeInfo> chageInfo;
    public bool cameraFocus = true;
    public bool unclosable = false;


    private List<string> lines = new List<string>();
    private int currentLine = 0;
    private PotaTween pop;
    private PotaTween fastPop;
    private bool pause = false;

    private BoxCollider2D boxCollider;
    private Transform ownTransform;
    private List<Option> clickedOptions = new List<Option>();

    private TextSize lineSize;
    private TextMesh backLine;

    void Awake ()
    {
        pop = PotaTween.Create(gameObject);
        pop.SetScale(Vector3.zero, Vector3.one);
        pop.SetEaseEquation(Ease.Equation.OutBack);
        pop.SetDuration(0.3f);

        fastPop = PotaTween.Create(gameObject,2);
        fastPop.SetScale(Vector3.one * .9f, Vector3.one);
        fastPop.SetEaseEquation(Ease.Equation.OutBack);
        fastPop.SetDuration(0.3f);

        boxCollider = GetComponent<BoxCollider2D>();
        ownTransform = transform;
       
        if (StringTable.deviceLanguage == SystemLanguage.Russian)
        {
            lineText.fontSize = 30;
            Font russianFont = Resources.Load("DejaVuSerifCondensed") as Font;
            lineText.font = russianFont;
            MeshRenderer rend = lineText.GetComponentInChildren<MeshRenderer>();
            rend.material = russianFont.material;
        }

        LoadTexts();

        PotaTween tween = PotaTween.Create(lineText.gameObject);
        tween.SetAlpha(0.0f, 1f);
        tween.SetEaseEquation(Ease.Equation.InSine);
        tween.SetDuration(0.5f);
    }

    private void LoadTexts()
    {
        string dialogTag = "NIGHT" + GameManager.Instance.currentDay + "." + name;

        bool allLinesAdded = false;
        int count = 0;

        

        string line = StringTable.GetText(dialogTag);

        lines.Clear();

        if (line != "NOT FOUND")
        {
            lines.Add(line);
        }
        
        while(!allLinesAdded)
        {
            line = StringTable.GetText(dialogTag + "." + count);

            if (line == "NOT FOUND")
            {
                break;
            }

            lines.Add(line);
            count++;
        }

        
    }

    public void Show()
    {
        if (GameManager.Instance.currentBox == null)
        {
            Open();
        }
    }

    public void ShowImmediately(float closeIn = 0)
    {
        Open();

        if (closeIn > 0)
        {
            Invoke("Hide", closeIn);
        }
    }

    private void Open()
    {
        if (pause) return;

        wasRead = true;
        gameObject.SetActive(true);
        pop.Play();
        boxCollider.enabled = true;
        ChangeLine();
        FMOD_StudioSystem.instance.PlayOneShot("event:/Dialogo/pop", ownTransform.position);

        StartCoroutine(WaitToShow());
    }

    void OnEnable()
    {
        foreach (Option item in options)
        {
            if (!clickedOptions.Contains(item))
            {
                item.gameObject.SetActive(true);
                continue;
            }
            item.gameObject.SetActive(false);
        }

    }

    private IEnumerator WaitToShow()
    {
        yield return new WaitForEndOfFrame();

        GameManager.Instance.DialogueMode(this);
        if (cameraFocus)
        {
            Camera.main.SendMessage(CameraMessages.FOCUS_ON, FocusData.BasicFocus(ownTransform.position));
        }
    }

    void OnMouseDown()
    {
        if (pause || unclosable) return;

        if (!hasOptions)
        {
            GoNext();
        }
    }

    public void SelectOption(Option option)
    {
        option.GetComponent<BoxCollider2D>().enabled = false;

        Transform optionTransform = option.transform;
        lineText = option.GetComponent<TextMesh>();
        chageInfo = option.chageInfo;
        lines = option.Lines;
        hasOptions = false;

        clickedOptions.Add(option);

        PotaTween movePos = PotaTween.Create(option.gameObject);
        Vector3 pos =  transform.position;
        pos.z = optionTransform.position.z;
        movePos.SetDuration(0.2f);
        movePos.SetPosition(optionTransform.position, pos);
        
        movePos.Play();
        currentLine++;

        foreach (Option item in options)
        {
            if (option != item)
            {
                item.gameObject.SetActive(false);
            }
        }
    }

    public void GoNext()
    {
        if (pause || pop.IsReversing) return;

        if (!fastPop.IsPlaying)
        {
            if (currentLine < lines.Count)
            {
                FMOD_StudioSystem.instance.PlayOneShot("event:/Dialogo/pop", transform.position);
                fastPop.Play();
                ChangeLine();

                GetComponent<BoxCollider2D>().enabled = false;
                Invoke("TurnOnCollider", 0.2f);

                return;
            }
        }

        Hide();
    }

    void TurnOnCollider()
    {
        GetComponent<BoxCollider2D>().enabled = true;
    }

    public void Hide()
    {
        if (pause) return;

        if (boxCollider.enabled)
        {
            GameManager.Instance.ClearCurrentBox();
            pop.Reverse(EndLines);
            boxCollider.enabled = false;
        }
    }

    private void ChangeLine()
    {
        if (pause) return;

        if (lines.Count > 0)
        {
            lineText.SendMessage("Play");
            lineText.text = lines[currentLine];   
            currentLine++;
        }
    }

   /* private void TypeLine()
    {
        lineText.text += lines[currentLine][lineText.text.Length];

        if(lineText.text == lines[currentLine])
        {
            currentLine++;
            return;
        }

        Invoke("TypeLine", 0.03f);
    }*/

    public void EndLines()
    {
        foreach (InteractableChangeInfo info in chageInfo)
        {
            if (info.interactable)
            {
                if (info.dialogueBox)
                {
                    info.interactable.SendMessage(DialogueMessages.CHANGE_DIALOGUE, info.dialogueBox);
                }
                else
                {
                    info.interactable.ClearDialogue();
                }
            }

            if (info.gameAction)
            {
                info.gameAction.DoAction();
            }

            if (info.gameAction2)
            {
                info.gameAction2.DoAction();
            }
        }

        Camera.main.SendMessage(CameraMessages.FOCUS_OFF);
        currentLine = 0;
        GameManager.Instance.InteractableMode();
        gameObject.SetActive(false);
    }

    public void ChangeAction(GameAction gameAction)
    {
        foreach (InteractableChangeInfo info in chageInfo)
        {
            info.gameAction = gameAction;
        }
    }

    public void ClearAction()
    {
        foreach (InteractableChangeInfo info in chageInfo)
        {
            info.gameAction = null;
        }
    }

    public void PauseOn()
    {
        pause = true;
    }

    public void PauseOff()
    {
        pause = false;
    }
}
