﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Option : MonoBehaviour {

    public List<string> Lines
    {
        get
        {
            return lines;
        }

        set
        {
            lines = value;
        }
    }

    public GameAction chooseAction;
    public List<InteractableChangeInfo> chageInfo;
    
    private List<string> lines = new List<string>();
    private TextMesh text;
    private Transform ownTransform;

    void Awake()
    {
        text = GetComponent<TextMesh>();
        ownTransform = transform;

        LoadTexts();

        if(lines.Count > 0){
            text.text = lines[0];
        }
    }

    public virtual void LoadTexts()
    {
        string dialogTag = "NIGHT" + GameManager.Instance.currentDay + "." + name;

        

        bool allLinesAdded = false;
        int count = 0;
        string line = StringTable.GetText(dialogTag);

        lines.Clear();

        if (line != "NOT FOUND")
        {
            lines.Add(line);
        }

        while (!allLinesAdded)
        {
            line = StringTable.GetText(dialogTag + "." + count);    

            if (line == "NOT FOUND")
            {
                break;
            }

            lines.Add(line);
            count++;
        }


    }

    void OnMouseDown()
    {
        if (ownTransform)
        {
            ownTransform.parent.SendMessage("SelectOption", this);

            if (chooseAction)
            {
                chooseAction.DoAction();
            }

            FloatingBehavior floating = GetComponent<FloatingBehavior>();
            ShakeBehavior shake = GetComponent<ShakeBehavior>();

            if (floating)
            {
                floating.StopFloat();
            }

            if (shake)
            {
                shake.StopShake();
            }
        }
    }
}
