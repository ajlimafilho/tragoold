﻿using UnityEngine;
using System.Collections;

public class Interactable : MonoBehaviour, IPausable {

    public bool IsSelected
    {
        get
        {
            return selected;
        }
    }

    public DialogueBox dialogueBox;
    public ThoughtBox thoughtBox;
    public GameAction gameAction;
    public GameObject sprite;
    public GameObject glowSprite;

    private bool selected= false;
    private BoxCollider2D boxCollider;
    private PotaTween pop;
    private bool pause = false;

    void Awake () {
        boxCollider = GetComponent<BoxCollider2D>();

		Vector3 spriteScale = sprite.transform.localScale;

        pop = PotaTween.Create(sprite);
		pop.SetScale(spriteScale * 0.95f, spriteScale);
        pop.SetEaseEquation(Ease.Equation.OutBack);
        pop.SetDuration(0.3f);
    }
    
    void FixedUpdate () {
    
    }

    void OnMouseDown()
    {
        if (pause) return;

        if (!selected)
        {
            pop.Play();

            selected = (dialogueBox != null || gameAction != null);

            if (thoughtBox)
            {
                thoughtBox.Hide();
            }
                 
            if (dialogueBox)
            {
                dialogueBox.Show();
            }

            if (gameAction)
            {
                gameAction.DoAction();

                if (!dialogueBox)
                {
                    selected = false;
                }
             }

            return;
        }

        if (dialogueBox)
        {
            if (dialogueBox.isActiveAndEnabled)
            {
                pop.Play();
                dialogueBox.GoNext();
            }
        }
    }

    void OnMouseEnter()
    {
        if (pause) return;

        if (!selected)
        {
            HighLightOn();
        }
    }

    private void HighLightOn()
    {
        if (thoughtBox)
        {
            thoughtBox.Show();
        }
    }

    void OnMouseExit()
    {
        if (pause) return;
        if (!selected)
        {
            HighLightOff();
        }
    }

    private void HighLightOff()
    {
        if (thoughtBox)
        {
            thoughtBox.Hide();
        }
    }

    public void ChangeDialogue(DialogueBox box)
    {
        dialogueBox = box;
    }

    public void ChangeAction(GameAction action)
    {
        gameAction = action;
    }

    public void ChangeThought(ThoughtBox thought)
    {
        thoughtBox = thought;
    }

    internal void TurnOff()
    {
        if (boxCollider == null)
        {
            boxCollider = GetComponent<BoxCollider2D>();
        }

        boxCollider.enabled = false;
    }

    public void TurnOn(bool glow = false)
    {
        boxCollider.enabled = true;
        selected = false;

        if (glow && glowSprite != null)
        {
           glowSprite.SetActive(true);
           glowSprite.SendMessage("Play");
        }
    }

    internal void SetSeletion(bool value)
    {
        selected = value;
    }

    public void ClearInterations()
    {
        dialogueBox = null;
        gameAction = null;
        thoughtBox = null;
    }

    public void ClearAction()
    {
        gameAction = null;
    }

    public void ClearDialogue()
    {
        dialogueBox = null;
    }

    public void ClearToughts()
    {
        thoughtBox = null;
    }

    private void NightEnd()
    {
        
    }

    public void PauseOn()
    {
        pause = true;
    }

    public void PauseOff()
    {
        pause = false;
    }
}
