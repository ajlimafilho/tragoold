﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class InteractableChangeInfo {

    public Interactable interactable;
    public DialogueBox dialogueBox;
    public GameAction gameAction;
    public GameAction gameAction2;
    
}
