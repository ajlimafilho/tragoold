﻿using UnityEngine;
using System.Collections;

public class ThoughtBox : MonoBehaviour, IPausable {

    public TextMesh textMesh;

    private PotaTween show;
    private PotaTween hide;
    private Transform ownTransform;
    private Vector3 savedPos;

    private bool isHide = true;
    private bool pause = false;

    void Awake () 
    {
        ownTransform = transform;
        savedPos = ownTransform.position;

        show = PotaTween.Create(gameObject);
        show.SetAlpha(0.0f,1f);
        //  show.SetPosition(TweenAxis.Y, ownTransform.position.y - GetComponent<SpriteRenderer>().bounds.size.y, ownTransform.position.y);
        show.SetEaseEquation(Ease.Equation.OutBack);
        show.SetDuration(0.5f);
        
        hide = PotaTween.Create(gameObject, 2);
        hide.SetAlpha(1f, 0.0f);
        hide.SetEaseEquation(Ease.Equation.InBack);
        hide.SetDuration(0.5f);

        textMesh.text = StringTable.GetText("NIGHT" + GameManager.Instance.currentDay + "." + name);
    }
    
    void Update () 
    {
    
    }

    public void Show()
    {
        if (pause) return;

        gameObject.SetActive(true);
        show.Play();
        isHide = false;

        Vector3 newPos = ownTransform.position;
        newPos.z = -2;

        ownTransform.position = newPos;
    }

    public void Hide()
    {
        if (hide)
        {
            ownTransform.transform.position = savedPos;
            gameObject.SetActive(false);
        }
    }

    private void CompleteHide()
    {
        
    }

    public void PauseOn()
    {
        pause = true;
        Hide();
    }

    public void PauseOff()
    {
        pause = false;
    }
}
