﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TragoOption : Option {

    public int addNumber = 0;

    public override void LoadTexts()
    {
        Lines = new List<string>();
        Lines.Clear();
        Lines.Add("" + (int.Parse(GameManager.Instance.glass.uiTragoCounter.text) + addNumber));
    }
    
}
