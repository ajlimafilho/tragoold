﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrinkQuestion : MonoBehaviour {

    public List<TextMesh> options;
    private List<string> bottleNames = new List<string>();

    void Start () {


        bottleNames.Add("GIM");
        bottleNames.Add("JURUBEBA");
        bottleNames.Add("WHISKEY");
        bottleNames.Add("CATUABA");
        bottleNames.Add("CACHACA");

        string currentBottle = GameManager.Instance.glass.CurrentBottle.ToUpper();
        currentBottle = currentBottle.Replace("Ç", "C");

        options[0].text = StringTable.GetText("BOTTLE."+ currentBottle);
        options[1].text = StringTable.GetText("BOTTLE." + RandomizeBottleName(currentBottle));
        options[2].text = StringTable.GetText("BOTTLE." + RandomizeBottleName(currentBottle));
    }
	
    public string RandomizeBottleName(string currentBottle)
    {
        string randomBottleName = currentBottle;

        int index = 0;

        while (randomBottleName == currentBottle)
        {
            index = Random.Range(0, bottleNames.Count - 1);
            randomBottleName = bottleNames[index];
        }

        bottleNames.Remove(randomBottleName);

        return randomBottleName;
    }


    void Update () {
	
	}
}
