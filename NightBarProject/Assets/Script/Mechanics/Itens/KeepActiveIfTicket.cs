﻿using UnityEngine;
using System.Collections;

public class KeepActiveIfTicket : MonoBehaviour {

	// Use this for initialization
	void Start () {
        gameObject.SetActive(GameManager.TICKET_ON);
    }
}
