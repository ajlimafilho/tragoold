﻿using UnityEngine;
using System.Collections;

public class KeepActiveIfMoney : MonoBehaviour {
    
	void Start () {
        gameObject.SetActive(GameManager.IS_MONEY_ABLE);
	}
}
