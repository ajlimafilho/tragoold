﻿using UnityEngine;
using System.Collections;

public class ActiveMoney : MonoBehaviour {

    public bool cheat;

    void Awake()
    {
        GameManager.IS_MONEY_ABLE = PlayerPrefs.GetString("IS_MONEY_ABLE") == "true";

        if (cheat)
        {
            MoneyOn();
        }

        gameObject.SetActive(GameManager.IS_MONEY_ABLE);
    }

    public void MoneyOn()
    {
        GameManager.IS_MONEY_ABLE = true;
        gameObject.SetActive(GameManager.IS_MONEY_ABLE);
        PlayerPrefs.SetString("IS_MONEY_ABLE", "true");
    }

    public void MoneyOff()
    {
        Debug.Log("MoneyOff");
        GameManager.IS_MONEY_ABLE = false;
        gameObject.SetActive(GameManager.IS_MONEY_ABLE);
        PlayerPrefs.SetString("IS_MONEY_ABLE", "false");
    }
    
}
