﻿using UnityEngine;
using System.Collections;

public class ActiveTicket : MonoBehaviour {

    public bool cheat;

    void Awake () 
    {
        GameManager.TICKET_ON = PlayerPrefs.GetString("IS_TICKET_ABLE") == "true";

        if (cheat)
        {
            Enable();
        }

        gameObject.SetActive(GameManager.TICKET_ON);
    }

    public void Enable()
    {
        GameManager.TICKET_ON = true;
        gameObject.SetActive(GameManager.TICKET_ON);
        PlayerPrefs.SetString("IS_TICKET_ABLE", "true");

    }

    public void Desable()
    {
        GameManager.TICKET_ON = false;
        gameObject.SetActive(GameManager.TICKET_ON);
        PlayerPrefs.SetString("IS_TICKET_ABLE", "false");
    }

}
