﻿using UnityEngine;
using System.Collections;

public interface IPausable
{
    void PauseOn();
    void PauseOff();
}
