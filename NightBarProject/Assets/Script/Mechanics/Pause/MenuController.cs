﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using System.Collections.Generic;

public class MenuController : MonoBehaviour {

    public BlurOptimized blur;
    public Image logo;
    public Image shadowTop;
    public Image shadowBottom;
    public Image tragoCounter;

    private PotaTween closeMenu;
    private PotaTween logoTween;
    private PotaTween shadowTopTween;
    private PotaTween shadowBottomTween;
    private PotaTween tragoCounterTween;

    private bool isOpen = false;
    private IList<IPausable> pausables;

    void Start ()
    {           
        closeMenu = PotaTween.Create(gameObject);
        logoTween = PotaTween.Create(logo.gameObject);
        shadowBottomTween = PotaTween.Create(shadowBottom.gameObject);
        shadowTopTween = PotaTween.Create(shadowTop.gameObject);

        if (tragoCounter)
        {
            tragoCounterTween = PotaTween.Create(tragoCounter.gameObject);
        }

        RectTransform shadowRect = shadowBottom.GetComponent<RectTransform>();
        Transform shadowTransform = shadowBottom.transform;
        float shadowPosY = shadowTransform.position.y + (Camera.main.ScreenToWorldPoint(shadowRect.rect.size).y);
        shadowBottomTween.SetPosition(TweenAxis.Y, shadowTransform.position.y, shadowPosY);
        shadowBottomTween.SetEaseEquation(Ease.Equation.OutSine);
        shadowBottomTween.SetDuration(0.2f);

        float topShadowPosY = shadowTop.transform.position.y - (Camera.main.ScreenToWorldPoint(shadowRect.rect.size).y);
        shadowTopTween.SetPosition(TweenAxis.Y, shadowTop.transform.position.y, topShadowPosY);
        shadowTopTween.SetEaseEquation(Ease.Equation.InSine);
        shadowTopTween.SetDuration(0.2f);

        RectTransform logoRect = logo.GetComponent<RectTransform>();
        Transform logoTransform = logo.transform;
        float logoPosY = logoTransform.position.y + (Camera.main.ScreenToWorldPoint(logoRect.rect.size).y);
        logoTween.SetPosition(TweenAxis.Y, logoTransform.position.y, logoPosY);
        logoTween.SetEaseEquation(Ease.Equation.OutBack);
        logoTween.SetDuration(0.3f);

        RectTransform menuRect = GetComponent<RectTransform>();
        
        Transform ownTransform = transform;
        float menuPosY = ownTransform.position.y + (Camera.main.ScreenToWorldPoint(menuRect.rect.size).y);
        closeMenu.SetPosition(TweenAxis.Y, ownTransform.position.y, menuPosY);
        closeMenu.SetEaseEquation(Ease.Equation.OutBack);
        closeMenu.SetDuration(0.3f);
        closeMenu.SetDelay(0.1f);

        Vector3 startPos = ownTransform.position;
        startPos.y = menuPosY;
        ownTransform.position = startPos;

        Vector3 logoStartPos = logoTransform.position;
        logoStartPos.y = logoPosY;
        logoTransform.position = logoStartPos;

        Vector3 shadowStartPos = shadowTransform.position;
        shadowStartPos.y = shadowPosY;
        shadowTransform.position = shadowStartPos;

        Vector3 shadowTopStartPos = shadowTop.transform.position;
        shadowStartPos.y = topShadowPosY;
        shadowTop.transform.position = shadowStartPos;

        pausables = InterfaceHelper.FindObjects<IPausable>();

    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (blur) {
                blur.enabled = true;
            }

            OpenCloseMenu();
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void OpenCloseMenu()
    {
        if (isOpen)
        {
            CloseMenu();
            return;
        }
        OpenMenu();
    }

    public void OpenMenu()
    {
        if (!closeMenu.IsPlaying && !isOpen)
        {
            if(AchievementController.Instance != null)
                AchievementController.Instance.ShowTheCurrentDayAchievementsOnPauseGame();

            pausables.Clear();
            pausables = InterfaceHelper.FindObjects<IPausable>();
            closeMenu.Reverse(EndOpenAnim);
            logoTween.Reverse();
            shadowBottomTween.Reverse();
            shadowTopTween.Reverse();

            isOpen = true;

            foreach (IPausable pauseObj in pausables)
            {
                pauseObj.PauseOn();
            }
        }
        
    }

    public void ShowTragoCounter()
    {
        tragoCounterTween.Stop();
        tragoCounterTween.Reverse();
    }

    public void HideTragoCounter()
    {
        tragoCounterTween.Stop();

        RectTransform tragoCounterRect = tragoCounter.GetComponent<RectTransform>();
        Transform tragoCounterTransform = tragoCounter.transform;
        float tragoCounterX = tragoCounterTransform.position.x - (Camera.main.ScreenToWorldPoint(tragoCounterRect.rect.size).x);
        
        tragoCounterTween.SetPosition(TweenAxis.X, tragoCounterTransform.position.x, tragoCounterX);
        tragoCounterTween.SetEaseEquation(Ease.Equation.OutBack);
        tragoCounterTween.SetDuration(1f);

        tragoCounterTween.Play();
    }

    public void EndOpenAnim()
    {

    }

    public void CloseMenu()
    {
        if (isOpen)
        {
            if (blur)
            {
                blur.enabled = false;
            }

            if (AchievementController.Instance)
            {
                AchievementController.Instance.DisableAchievementPanel();
            }

            closeMenu.Play();
            logoTween.Play();
            shadowBottomTween.Play();
            shadowTopTween.Play();
            isOpen = false;

            foreach (IPausable pauseObj in pausables)
            {
                pauseObj.PauseOff();
            }
        }
    }

    public void ShowAchievementsWindow()
    {
        AchievementController.Instance.ShowAllAchievementsOnAchievementWindow();
    }
}
