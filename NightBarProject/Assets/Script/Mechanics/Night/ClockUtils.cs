﻿using UnityEngine;
using System.Collections;

public class ClockUtils : MonoBehaviour {

    public static string MinuteTimer(float time)
    {
        float min = (Mathf.Floor((time / 60))) % 24;
        float sec = Mathf.Floor((time % 60));

        string clock = FormatTime(min) + ":" + FormatTime(sec);

        return clock;
    }

    public static string HourTimer(float time)
    {
        float sec = Mathf.Floor((time % 60));
        float min = Mathf.Floor((time / 60));
        float hour = Mathf.Floor((min / 60));
        float mili = ((time % 60) - sec) * 1000;

        string clock = FormatTime(hour) + ":" + FormatTime(min) + ":" + FormatTime(sec);

        if (time == 0)
        {
            return "--:--:--";
        }

        return clock;
    }

    private static string FormatTime(float time)
    {
        string text = ""+time;
        
        if (text.Length == 1)
        {
            text = "0" + text;
        }

        if (text.Length > 2)
        {
            text = text.Remove(2);
        }

        return text;
    }

    internal static string SecondTime(float time)
    {
        float sec = Mathf.Floor((time % 60));
        float mili = ((time % 60) - sec) * 1000;

        string clock = FormatTime(sec) + "," + FormatTime(mili);

        return clock;
    }
}
