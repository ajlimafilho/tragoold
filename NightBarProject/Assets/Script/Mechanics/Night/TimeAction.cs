﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TimeAction {
    public float time;
    public GameAction action;
}
