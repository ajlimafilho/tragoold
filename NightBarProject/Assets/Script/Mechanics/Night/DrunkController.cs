﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class DrunkController : MonoBehaviour {

    //public Vector2 bounds;
    public float maxBlur = 1;
    public float increaseBlur;
    public float decreaseBlur;

    private MotionBlur motion;
    private float blur;
    private bool canDecrease = false;

    void Start () {
        motion = GetComponent<MotionBlur>();

        blur = 0;
        motion.blurAmount = blur;
    }

    void Update()
    {
        if (canDecrease)
        {
            float blurUpdated = blur - (decreaseBlur * Time.deltaTime);
            blurUpdated = Mathf.Clamp(blurUpdated, 0, maxBlur);

            if (blurUpdated != blur)
            {
                blur = blurUpdated;
                motion.blurAmount = blur; 
                
                
            }
        }
    }

    public void AddBlur()
    {
        blur += increaseBlur;
        motion.blurAmount = blur;
        canDecrease = false;
    }

    public void StartDecrease()
    {
        canDecrease = true;
    }
}
