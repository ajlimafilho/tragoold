﻿using UnityEngine;
using System.Collections;

public class CustomActions : MonoBehaviour
{
    public GameAction noTicketAction;
    public GameAction ticketAction;

    public GinGlass ginGlass;
    public DialogueBox endDialogue;
    public GameAction ringPhone;
    public GameAction stopRingPhone;
    public GameAction carlosShowUpAction;

    public CloseDrinkGame closeDrinkGame;

    private bool phoneRings = false;
    private bool stopRings = false;
    private bool tvOn = false;
    private bool canCarlosShowUp = false;

    void Start () {
    
    }
    
    void Update () 
    {
    
    }

    public void BuyTicket()
    {
        if (GameManager.IS_MONEY_ABLE)
        {
            ticketAction.DoAction();
            return;
        }

        noTicketAction.DoAction();
    }

    public void HaveTicket()
    {
        if (GameManager.TICKET_ON)
        {
            ticketAction.DoAction();
            return;
        }

        noTicketAction.DoAction();
    }

    public void NoRing()
    {
        phoneRings = true;
        stopRings = true;
    }

    public void DoPhoneRing()
    {
        if (!phoneRings)
        {
            /*if (GameManager.Instance.gameMode == GameManager.GameMode.DRINK)
            {
                closeDrinkGame.OncloseDrinkGameClicked();
            }*/

            ringPhone.DoAction();
            phoneRings = true;
        }
    }

    public void StopPhoneRing()
    {
        if (!stopRings)
        {
            stopRingPhone.DoAction();
            stopRings = true;
        }
    }

    public void End()
    {
        GameManager gameManager = GameManager.Instance;
        if (gameManager.GMode == GameManager.GameMode.DRINK)
        {
            closeDrinkGame.OncloseDrinkGameClicked();
        }

        GameManager.Instance.CloseDialogueMode();
        endDialogue.ShowImmediately();
    }

    public void ShowDialogue()
    {
        endDialogue.ShowImmediately();
    }

    public void CanCarlosShowUp(bool value)
    {
        canCarlosShowUp = value;
    }

    public void CarlosShowUp()
    {
        if (canCarlosShowUp && carlosShowUpAction != null)
        {
            carlosShowUpAction.DoAction();
            
        }
    }
}
