﻿using UnityEngine;
using System.Collections;

public class Night5Actions : MonoBehaviour
{

    public GameObject News;
    public Animator TvAnimator;
    private PotaTween newsTween;
    public DialogueBox NewsDiaogue;
    public GameObject BottleGin;
    public BoxCollider2D BottleGinCollider;
    private bool canScale;
    private SpriteRenderer ginBottleColor;
    private Color32 ginLightBlackColor;
    public GinGlass ginGlass;

    void Start ()
    {
        ginGlass.LostCombo();
        ginGlass.finalDrink = true;
        Invoke("DisableBottleCollider", .4f); //this gambi cause gamemanager enable all interactable objects after.3f

        //ginBottleColor = BottleGin.GetComponent<SpriteRenderer>();
        //ginLightBlackColor = new Color32(180, 180, 180, 255);

        newsTween = PotaTween.Create(News);
        newsTween.SetDuration(.5f);
        newsTween.SetAlpha(0f, 1f);
        newsTween.SetEaseEquation(Ease.Equation.Linear);

        StartCoroutine(ShowNewsCoroutine());
    }


    IEnumerator ShowNewsCoroutine()
    {
        yield return new WaitForSeconds(2);

        TvAnimator.SetTrigger("news");
        newsTween.Play();

        yield return new WaitForSeconds(.5f);
        NewsDiaogue.Show();
    }

    void DisableBottleCollider()
    {
        BottleGinCollider.enabled = false;
    }

    public void EnableBottle()
    {
        canScale = !canScale;
        BottleGinCollider.enabled = canScale;
    }

    void Update ()
    {
        if (canScale)
        {
            BottleGin.transform.localScale = Vector3.Lerp(Vector3.one * 0.95f, Vector3.one * 1.05f, Mathf.PingPong(Time.time, 1));
            //ginBottleColor.color = Color.Lerp(ginLightBlackColor, Color.white, Mathf.PingPong(Time.time, 1));
        }
    }
}
