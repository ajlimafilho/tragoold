﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FinalText : MonoBehaviour
{

    public TextMesh finalText;
    private int drinksDrinked;

    void Start ()
    {
        drinksDrinked = GameManager.HowManyDrinks;
        FinalMessage();
    }
         
    void FinalMessage ()
    {
        if (drinksDrinked >= 25)
        {
            if (StringTable.deviceLanguage == SystemLanguage.English)
            {
                finalText.text = "*You've scaped alive after drinking <color=white>" + drinksDrinked + "</color> tragos. You are a BADASS!";
            }
            else
            {
                finalText.text = "*Você escapou vivo após beber <color=white>" + drinksDrinked + "</color> tragos. Você comanda!";
            }
            return;
        }
        if (drinksDrinked >= 15)
        {
            if (StringTable.deviceLanguage == SystemLanguage.English)
            {
                finalText.text = "*It was nice to drink these <color=white>" + drinksDrinked + "</color> shots with you";
            }
            else
            {
                finalText.text = "*Foi legal beber esses <color=white>" + drinksDrinked + "</color> tragos com você.";
            }
            return;
        }
        else
        {
            if (StringTable.deviceLanguage == SystemLanguage.English)
            {
                finalText.text = "*You've drank only <color=white>" + drinksDrinked + "</color> tragos, at least you still alive";
            }
            else
            {
                finalText.text = "*Você bebeu apenas <color=white>" + drinksDrinked + "</color> tragos, pelo menos está vivo.";
            }
            return;
        }
    }
}
