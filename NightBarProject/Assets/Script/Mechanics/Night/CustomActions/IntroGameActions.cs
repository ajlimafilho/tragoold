﻿using UnityEngine;
using System.Collections;

public class IntroGameActions : MonoBehaviour {

    public static bool PLAY_CREDITS = false;

    public GameAction whenMoneyAble;

    public GameAction playCredits;
    public GameAction cutCredits;

    void Start () {
        if (GameManager.IS_MONEY_ABLE)
        {
            whenMoneyAble.DoAction();
        }
    }
    
    void Update () 
    {
    
    }

    public void StartIntro()
    {
        if (IntroGameActions.PLAY_CREDITS)
        {
            playCredits.DoAction();
            return;
        }

        GameManager.Instance.GetComponent<NightTimeController>().nightMask.SetActive(true);
        cutCredits.DoAction();
    }
}
