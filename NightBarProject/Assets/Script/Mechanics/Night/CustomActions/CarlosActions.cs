﻿using UnityEngine;
using System.Collections;

public class CarlosActions : MonoBehaviour {

    public GameObject carlos;
    public DialogueBox carlosOutDialogue;
    public GameAction carlosOutAction;
    public DialogueBox carlosDrunkEnd;

	public GameAction carlosBackAction;
    public GameAction carlosShootAction;
    public DialogueBox carlosDialogue;
	public GameAction carlosMoneyEnd;

    private bool IsCarlosOut = false;

    // Use this for initialization
    void Start () {
    
    }
    
    // Update is called once per frame
    void Update () {
    
    }

    public void CarlosKill()
    {
        if (GameManager.Instance.gameMode == GameManager.GameMode.DRINK)
        {
            
        }

        if (IsCarlosOut)
        {
            GameManager manager = GameManager.Instance;

            if (manager.currentBox)
            {
                manager.currentBox.ClearAction();
                manager.CloseDialogueMode();
            }

            carlosBackAction.DoAction();
            Invoke("PlayEndDialogue", 2f);
            IsCarlosOut = false;
        }
        
    }

    private void PlayEndDialogue()
    {
        carlosDialogue.ShowImmediately();
    }

    public void CarlosOut()
    {
        if (!IsCarlosOut)
        {
            IsCarlosOut = true;
            GameManager manager = GameManager.Instance;
            if (manager.GMode == GameManager.GameMode.DRINK)
            {
                carlosOutAction.DoAction();
                return;
            }

            if (manager.currentBox)
            {
                manager.currentBox.ClearAction();
                manager.CloseDialogueMode();
            }

            carlosOutDialogue.ShowImmediately(3f);
        }

    }

    public void EndFocus(Transform target)
    {
        Camera.main.SendMessage(CameraMessages.FOCUS_ON, FocusData.HitFocus(target.position));
    }

    public void EndDrunk()
    {
        if (!IsCarlosOut)
        {
            carlosDrunkEnd.Show();
        }
    }

    public void SetCarlosOut(bool value)
    {
        IsCarlosOut = value;
    }

	public void MoneyEnd()
	{
		if (IsCarlosOut) 
		{
			carlosBackAction.DoAction ();
		}

		Invoke ("ShowMoneyDialogues", 2.0f);
	}

	public void ShowMoneyDialogues()
	{
		if (!IsCarlosOut) 
		{
			carlosShootAction.DoAction ();
		}

		carlosMoneyEnd.DoAction ();
	}
}
