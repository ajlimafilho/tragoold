﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ClockManager : MonoBehaviour, IPausable {

    private const float BASE_TIME = 1320;
    private const int DAY_NUMBER = 4;
    private const float ADVANCE_TIME_VALUE = 10;

    public float clockScale = 0.5f;

    public float CurrentTime
    {
        get
        {
            return time;
        }
    }


    public Text dayCount;
    public Text hourCount;
    public bool fastNight;
    public float criticalTime = 30;
    public Button clockButton;

    private float nightTime = 180;
    private float time;
    private bool stopClock = true;
    private bool backTime = false;
    private bool advanceTime = false;

    private float cumulativeBacktime;
    private int backdays = 4;
    private float resetTime = 0;
    private PotaTween fastPop;
    private PotaTween clockPop;
    private float lastSec = 0;
    private bool pause = false;
    private int dayIndex = 0;

    void Start () {
        if (fastNight)
        {
            nightTime = 10;
        }

        fastPop = PotaTween.Create(gameObject, 2);
        fastPop.SetScale(Vector3.one * .9f, Vector3.one);
        fastPop.SetEaseEquation(Ease.Equation.OutBack);
        fastPop.SetDuration(0.3f);

        clockButton.onClick.AddListener(OnClockClick);

        clockPop = PotaTween.Create(gameObject);
        clockPop.SetScale(Vector3.one * .75f, Vector3.one);
        clockPop.SetEaseEquation(Ease.Equation.OutBack);
        clockPop.SetDuration(0.4f);

        //clockScale = 0.5f; 
    }
    
    public void StartNight()
    {
        hourCount.text = "22:00";
        stopClock = false;
        time = 0;
    }
    
    void FixedUpdate () {
        if (pause) return;

        if (!stopClock)
        {
            if (time < nightTime)
            {
                
                time += Time.fixedDeltaTime * clockScale;
                hourCount.text = ClockUtils.MinuteTimer(BASE_TIME + time);

                if (ChangeSec())
                {
                    fastPop.Play();
                }

                return;
            }

            stopClock = true;
            GameManager.Instance.SendMessage(GameMessages.CLOSE_PUB);
        }

        if (advanceTime)
        {
            resetTime += ADVANCE_TIME_VALUE + 2;
            hourCount.text = ClockUtils.MinuteTimer(resetTime);

            if (resetTime >= BASE_TIME)
            {
                advanceTime = false;
            }
        }

        if (backTime)
        {
            resetTime -= ADVANCE_TIME_VALUE *4;
            cumulativeBacktime -= ADVANCE_TIME_VALUE * 4;

            hourCount.text = ClockUtils.MinuteTimer(resetTime);

            if (resetTime <= 0)
            {
                backdays--;
                dayCount.text = "Day " + backdays;
                resetTime = BASE_TIME + nightTime;
            }

            if (cumulativeBacktime <= 0)
            {
                GameManager.Instance.ChangeScene(dayIndex);
                hourCount.text = "22:00";
                backTime = false;
            }

        }
        
    }

    private bool IsInCritical()
    {
        return criticalTime > (nightTime - Mathf.Floor(time % 60));
    }

    private bool ChangeSec()
    {
        if (lastSec != Mathf.Floor(time))
        {
            lastSec++;
            return true;
        }

        return false;
    }

    public void ResetClock()
    {
        resetTime = 0;
        advanceTime = true;
    }

    public void PlayClock()
    {
        stopClock = false;
    }

    public void StopClock()
    {
        stopClock = true;
    }

    public void BackDays(int dayIndex)
    {
        this.dayIndex = dayIndex;
        stopClock = true;
        resetTime = BASE_TIME + time;
        backTime = true;
        cumulativeBacktime = ((BASE_TIME + nightTime) * (DAY_NUMBER - 1)) + time;
    }

    public void SetClockScale(float clockScale)
    {
        this.clockScale = clockScale;
    }

    public void PauseOn()
    {
        pause = true;
    }

    public void PauseOff()
    {
        pause = false;
    }

    public void OnClockClick()
    {
        clockPop.Play();
    }
}
