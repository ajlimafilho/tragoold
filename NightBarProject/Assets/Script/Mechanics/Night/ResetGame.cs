﻿using UnityEngine;
using System.Collections;

public class ResetGame : MonoBehaviour {

    public GameAction firstDeadAction;
    public GameObject finalMenuMask;
    public GameObject gameJoltEnd;
    
    void Start () 
    {
        
    }

    public void EndLastDay()
    {
        
        GameManager.Instance.SendMessage("PlayOneShot", "tiro");

        if (!PremiumSetup.SKIP_BASIC_SETUP)
        {
            firstDeadAction.DoAction();
            return;
        }

        finalMenuMask.SetActive(true);
    }
}
