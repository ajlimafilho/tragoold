﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NightTimeController : MonoBehaviour {

    public static int NIGHT_COUNT = 1;
    public static bool NIGHT_IS_ROLLING = false;

    public TextMesh title;
    public List<GameAction> endNightActions;
    public List<TimeAction> timeActions = new List<TimeAction>();
    public List<GameAction> endDrunkNightActions = new List<GameAction>();

    public bool jucaCanFallDrunk;
    public ClockManager clock;
    public GameObject logoBack;
    public GameObject logoFront;
	public GameObject shelf;

    public GameObject nightMask;
    public GameAction badEndActions;
    public GameObject jornal;
    public Material HSVMaterial;

    private int sceneNights = 0;
    private PotaTween nightMaskTween;
    private PotaTween titleTween; 
    private PotaTween titleTween2;
    private PotaTween clockTween;
    private TimeAction currentTimeAction;
    private PotaTween jornalTween;
    private PotaTween saturationTween;
    private bool nightEnd = false;
    private bool drunkEnd = false;

    void Awake()
    {
        HSVMaterial.SetFloat("_Sat", 1);

        NIGHT_IS_ROLLING = true;

        if (WeeklySetup.WEEKLY_ON)
        {
            if (nightMask != null)
            {
                NIGHT_IS_ROLLING = false;
            }
        }

        nightMask.SetActive(NIGHT_IS_ROLLING);
    }

    void Start()
    {
        if (nightMask != null)
        {
            nightMaskTween = PotaTween.Create(nightMask);
            nightMaskTween.SetPosition(TweenAxis.Y, nightMask.transform.position.y, 11);
            nightMaskTween.SetDelay(2f);
            nightMaskTween.SetDuration(4f);
        }

        if (clock)
        {
            clockTween = PotaTween.Create(clock.gameObject);
            clockTween.SetAlpha(0.0f, 1.0f);

            clockTween.SetDuration(0.5f);
        }

        if (jornal)
        {
           jornalTween = PotaTween.Create(jornal);
        }

        if (title)
        {
            titleTween = PotaTween.Create(title.gameObject);
            titleTween.SetAlpha(0.0f, 1.0f);
            titleTween.SetLoop(LoopType.PingPong);
            titleTween.SetDuration(1f);
            titleTween.SetEaseEquation(Ease.Equation.OutSine);
            titleTween.LoopsNumber = 2;
            sceneNights = 0;

            titleTween.SetDuration(4f);

            titleTween2 = PotaTween.Create(title.gameObject, 2);
            titleTween2.SetDuration(0.5f);
            titleTween2.SetScale(Vector3.one * 0.5f, Vector3.one);
            titleTween2.SetEaseEquation(Ease.Equation.OutBack);

            titleTween.Play();
        }
        

        Invoke("OnShowTitleComplete", 1f);
        
        if (clock)
        {
            clock.StartNight();
        }

        FindNextTimeAction();

    }

    private void OnShowTitleComplete()
    {
        if (nightMask == null) return;

        if (nightMask.activeInHierarchy)
        {
            nightMask.SendMessage("Open");
        }
    }

    void FixedUpdate()
    {
        if (clock)
        {
            GameManager.Instance.AmbienceClockSound(clock.CurrentTime * 0.01f);
        }

        if (currentTimeAction != null && !drunkEnd)
        {

            if (clock.CurrentTime > currentTimeAction.time)
            {
                currentTimeAction.action.DoAction();
                currentTimeAction = null;
                FindNextTimeAction();
            }
        }
    }

    private void NightEnd()
    {   
        
        if (!nightEnd)
        {
            List<GameAction> actions;

            if (drunkEnd)
            {
                actions = endDrunkNightActions;
            }
            else
            {
                actions = endNightActions;
            }

            if (actions.Count > 0)
            {
                int count = 0;

                while (count < actions.Count)
                {
                    actions[count].DoAction();
                    count++;
                }
                nightEnd = true;

                if (GameManager.Instance.currentDay == 4 && drunkEnd)
                {
                    return;
                }

                SendMessage("FinishNight");
            }
        }
    }

    internal void DrunkEnd()
    {
        drunkEnd = true;
    }

    public void FinishNight()
    {
        if (title)
        {
            title.gameObject.SetActive(false);
        }

        GameManager.Instance.StopMusic();

        if (IntroGameActions.PLAY_CREDITS)
        {
            IntroGameActions.PLAY_CREDITS = false;
        }

        NightTimeController.NIGHT_COUNT++;
        nightMask.gameObject.SetActive(true);

        TurnToBlackAndWhite();
        //nightMaskTween.Reverse(TimePass);

        nightMask.SendMessage("Close");
    }

	public void FinishNight(bool turnBlack = true)
	{
		if (title)
		{
			title.gameObject.SetActive(false);
		}

		GameManager.Instance.StopMusic();

		if (IntroGameActions.PLAY_CREDITS)
		{
			IntroGameActions.PLAY_CREDITS = false;
		}

		NightTimeController.NIGHT_COUNT++;
		nightMask.gameObject.SetActive(true);

		if (turnBlack) {
			TurnToBlackAndWhite();
		}
		//nightMaskTween.Reverse(TimePass);

		nightMask.SendMessage("Close");
	}

    public void TurnToBlackAndWhite()
    {
        saturationTween = PotaTween.Create(gameObject);
        saturationTween.SetFloat(1, 0);
        saturationTween.UpdateCallback(ApplyToMaterial);
        saturationTween.SetDuration(1);
        saturationTween.Play();
    }

    private void ApplyToMaterial()
    {
        HSVMaterial.SetFloat("_Sat", saturationTween.Float.Value);
    }

    public void FinishNightCloseAll()
    {
        
        GameManager.Instance.StopMusic();

        if (IntroGameActions.PLAY_CREDITS)
        {
            IntroGameActions.PLAY_CREDITS = false;
        }
        if(title)
            title.gameObject.SetActive(false);
        NightTimeController.NIGHT_COUNT++;
        StartCoroutine(DrunkActionsAndEndNight());
    }

    IEnumerator DrunkActionsAndEndNight()
    {
        if(jucaCanFallDrunk)
        {
            yield return new WaitForSeconds(4f);
            for (int i = 0; i < endDrunkNightActions.Count; i++)
            {
                endDrunkNightActions[i].DoAction();

            }
            yield return new WaitForSeconds(1f);
        }
        nightMask.gameObject.SetActive(true);
        nightMask.SendMessage("CloseAll");
    }

    public void BadNightEnd()
    {
        logoBack.SetActive(false);
        badEndActions.DoAction();
        nightEnd = true;
        NightTimeController.NIGHT_COUNT = 1;
    }

    public void GoodNightEnd()
    {
        nightMask.GetComponent<SpriteRenderer>().color = Color.black;

        logoBack.SetActive(false);
        jornalTween.SetRotation(TweenAxis.Z, 0, 720);
        jornalTween.SetScale(Vector3.zero, Vector3.one * 1.2f);
        jornalTween.SetDuration(0.8f);
        jornalTween.SetEaseEquation(Ease.Equation.OutSine);
        jornalTween.SetDelay(0.5f);

        GameManager.Instance.StopMusic();

        jornal.SetActive(true);
        nightMask.SetActive(true);

        clock.StopClock();
        nightMaskTween.Clear();
        nightMaskTween.SetAlpha(0.0f, 1f);
        nightMaskTween.SetDuration(1f);
        nightMask.transform.position = Vector2.zero;
        nightMaskTween.Play();
        clockTween.Play();
        jornalTween.Play(BlackComplete);
    }

    private void BlackComplete()
    {
        nightMaskTween.Clear();
        nightMaskTween.SetColor(Color.black, Color.white);
        nightMaskTween.SetDuration(1f);
        nightMaskTween.Play();

        jornalTween.Clear();
        jornalTween.SetAlpha(1f, 0f);
        jornalTween.SetDuration(0.5f);
        jornalTween.SetDelay(2f);

        jornalTween.Play();

        Invoke("WinGame",3f);
    }

    private void WinGame()
    {
        NightTimeController.NIGHT_COUNT = 5;
        GameManager.Instance.SendMessage(GameMessages.NEXT_SCENE);
    }

    public void BackDays(int dayIndex)
    {
        IntroGameActions.PLAY_CREDITS = false;
        PremiumSetup.SKIP_BASIC_SETUP = true;

        if (title)
        {
            titleTween2.Play();
        }

        GameManager.Instance.StopMusic();

        Invoke("ChangeText", 1.5f);

        clock.BackDays(dayIndex);

        nightMaskTween.Clear();
        nightMaskTween.SetPosition(TweenAxis.Y, nightMask.transform.position.y, 0);
        nightMaskTween.SetDuration(1f);
        nightMaskTween.Play();
        PotaTween logoTween = PotaTween.Create(logoFront);
        logoTween.SetAlpha(0f, 1f);
        logoTween.SetDuration(1f);
        logoFront.SetActive(true);
        logoTween.Play();
    }

    private void ChangeText()
    {
        if (title)
        {
            titleTween.Play();
            title.text = "But you can change that.";
        }

        nightMaskTween.Clear();
        nightMaskTween.SetColor(Color.black, Color.white);
        nightMaskTween.SetDuration(1f);
        nightMaskTween.Play();
    }

	private void OpenTheShelf()
	{
		if (clock) 
		{
			clock.StopClock ();
		}

		shelf.SendMessage ("Play");
        AchievementController.Instance.AchievementComplete(10);
	}

    private void TimePass()
    {
        clock.ResetClock();

        if (title)
        {
            titleTween.Play();
        }

        Invoke("ReturnGameplay",2f);
    }

    private void ReturnGameplay()
    {
        if (title)
        {
            titleTween.Reverse();
        }

        Invoke("EndScene", 0.5f);
   } 

    private void EndScene()
    {
        SendMessage(GameMessages.NEXT_SCENE);
    }

    private void FindNextTimeAction()
    {
        if (clock == null) return;

        int count = 0;

        while (timeActions.Count > count)
        {
            float actionTime = timeActions[count].time;
            bool isOutOfTime = clock.CurrentTime > actionTime;

            if (currentTimeAction != null)
            {
                if (!isOutOfTime && currentTimeAction.time > actionTime)
                {
                    currentTimeAction = timeActions[count];
                }
                count++;

                continue;
            }

            if (!isOutOfTime)
            {
                currentTimeAction = timeActions[count];
            }

            count++;
        }
    }
}
