﻿using UnityEngine;
using System.Collections;

public class ScoreScreen : MonoBehaviour {

    private static ScoreScreen instance;

    public static ScoreScreen Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<ScoreScreen>();

            return instance;
        }
    }

    public int tableID;
    public TextMesh totalDrinking;
    public TextMesh totalCombo;
    //public TextMesh totalSequence;
    public TextMesh totalScore;
    public TextMesh thankYou;
    public GameObject logo;

    private int sequence;
    private int combo;

    void Start () 
    {
        totalDrinking.text = StringTable.GetText("SCORE.TRAGO").Replace("[TRAGOS]", GameManager.HowManyDrinks.ToString());

        int sequence = GameManager.Instance.glass.MaxSequenceCombo;
        int combo = GameManager.Instance.glass.MaxCombo;

        //totalSequence.text = sequence.ToString();
        totalCombo.text = combo.ToString();

        int score = sequence;

        totalScore.text = (score).ToString();

        int scoreValue = score;

        if (GameManager.IsWeeklyVersion())
        {
            int continues = WeeklySetup.TakeContinue();

            thankYou.text = StringTable.GetText("SCORE.CONTINUE") + " x" + continues;

            if (continues == 0)
            {
                thankYou.text = StringTable.GetText("SCORE.TO_QUIT");
                thankYou.GetComponent<BoxCollider2D>().enabled = false;
            }


            WeeklySetup.SaveScoreData(scoreValue, tableID);
            return;
        }

        thankYou.text = StringTable.GetText("NEXT_DAY");
    }

    public void GameJoltSetup(int scoreValue)
    {
    }

    public void RestartGame()
    {
        SendMessage("Reverse");
        
        if (logo)
        {
            logo.SetActive(true);
            PotaTween logoTween = PotaTween.Create(logo, 2);
            logoTween.SetAlpha(0f, 1F);
            logoTween.SetDuration(0.5f);

            if (GameManager.IsWeeklyVersion())
            {
                logoTween.Play(Restart);
                return;
            }

            logoTween.Play(NextEpisode);
        }
    }

    private void NextEpisode()
    {
        GameManager.Instance.NextScene();
    }

    private void Restart()
    {
        GameManager.Instance.RestartGame();
    }

    void Update () 
    {

    }
}
