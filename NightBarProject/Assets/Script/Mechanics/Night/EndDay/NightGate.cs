﻿using UnityEngine;
using System.Collections;
using FMOD.Studio;

public class NightGate : MonoBehaviour {

    public GameObject logo;
    public GameObject logoOff;
    public GameObject scoreScreen;
    public GameObject dragWarning;

    private PotaTween openTween;
    private PotaTween popTween;
    private PotaTween autoClose;

    private BoxCollider2D collider2D;

    private Transform ownTransform;
    private Bounds bounds;

    private bool dragOn = false;
    private bool endNight = false;

    private EventInstance sfx;

    void Awake () 
    {
        openTween = PotaTween.Create(gameObject);

        openTween.SetPosition(TweenAxis.Y, transform.position.y, 11);
        openTween.SetEaseEquation(Ease.Equation.OutSine);
        openTween.SetDuration(5f);

        collider2D = GetComponent<BoxCollider2D>();
        bounds = GetComponent<SpriteRenderer>().bounds;

        ownTransform = transform;

        popTween = PotaTween.Create(gameObject,2);
        popTween.SetPosition(TweenAxis.Y, 11, 9.5f);
        popTween.SetEaseEquation(Ease.Equation.OutBack);
        popTween.SetDuration(1f);

       
    }

    void Start()
    {
        sfx = FMOD_StudioSystem.instance.GetEvent("event:/portao");
    }

    void OnMouseOver()
    {
        if (endNight) return;

        bool canDrag = Input.GetMouseButtonDown(0) && !openTween.IsPlaying;

        if (canDrag)
        {
            dragOn = true;
        }
    }
    
    void Update () 
    {
        if (Input.GetMouseButtonUp(0))
        {
            dragOn = false;
        }

        if (dragOn)
        {
            Vector3 newPos = ownTransform.position;
            newPos.y = (Camera.main.ScreenToWorldPoint(Input.mousePosition).y + (bounds.size.y * 0.5f) - 1f);
            ownTransform.position = newPos;

            if (ownTransform.position.y < 1.2f)
            {
                dragWarning.SetActive(false);
                endNight = true;

                autoClose = PotaTween.Create(gameObject, 3);
                autoClose.SetPosition(TweenAxis.Y, transform.position.y, 0);
                autoClose.SetDuration(1f);

                autoClose.SetEaseEquation(Ease.Equation.OutSine);
                autoClose.Play(CloseComplete);

                dragOn = false;
            }
        }
    }

    private void CloseComplete()
    {
        //TODO: Mudar essa lógica.
       /* if (GameManager.GJON == GameManager.VERSION_OFF)
        {
            logo.SetActive(true);

            PotaTween alphaTween = PotaTween.Create(logo);
            alphaTween.SetAlpha(0.0F, 1.0F);
            alphaTween.SetEaseEquation(Ease.Equation.OutElastic);

            alphaTween.SetDuration(1);

            alphaTween.Play(ReturnGameplay);
            return;
        }*/

        logo.SetActive(false);
        logoOff.SetActive(false);
        scoreScreen.SetActive(true);
    }

    private void ReturnGameplay()
    {
        GameManager.Instance.SendMessage("ReturnGameplay");
    }

    public void OnClickIn()
    {

    }

    public void TriggerEnd()
    {

    }

    public void Open()
    {
        openTween.Play(OpenComplete);
        Invoke("PlaySound", 2f);
    }

    private void PlaySound()
    {
        sfx.setParameterValue("abreFecha", 0f);
        sfx.start();
    }

    private void OpenComplete()
    {
        sfx.setParameterValue("abreFecha", 1f);
        logo.SetActive(false);
    }

    public void Close()
    {
        dragWarning.SetActive(true);
        popTween.Play();

    }

    public void CloseAll()
    {
        openTween.Reverse(CloseComplete);
        sfx.setParameterValue("abreFecha", 1f);
    }

    public void CloseAndReturnToMenu()
    {
        if (!WeeklySetup.WEEKLY_ON)
        {
            PremiumSetup.SKIP_BASIC_SETUP = true;
            openTween.Reverse(BackToMenu);
        }
        
        sfx.setParameterValue("abreFecha", 1f);
    }

    public void BackToMenu()
    {
        Application.LoadLevel(0);
    }

}
