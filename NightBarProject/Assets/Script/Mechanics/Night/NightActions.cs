﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class NightActions {

    public List<GameAction> actions = new List<GameAction>();

    internal void PlayActions()
    {
        foreach (GameAction action in actions)
        {
            action.DoAction();
        }
    }
}
