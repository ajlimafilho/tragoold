﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextKeysDisplay : MonoBehaviour {

    private const float KEY_DIMENTIONS = 0.8f;
    private const float QUEUE_END_POS_X = 0.4f;

    public bool IsEmpty
    {
        get
        {
            return (codeQueue.Count == 0);
        }
    }

    public GameObject keyModel;
    public int size;

    private Queue<GameObject> keysQueue;
    private Queue<PointerData> codeQueue;

    public void Setup(List<PointerData> steps)
    {
        codeQueue = new Queue<PointerData>();
        keysQueue = new Queue<GameObject>();

        for (int i = 0; i < steps.Count; i++)
        {
            codeQueue.Enqueue(steps[i]);
        }


        int countSize = 0;
        foreach (PointerData data in codeQueue)
        {
            GameObject key = GameObject.Instantiate(keyModel);
            TextMesh keyText = key.GetComponent<TextMesh>();
            Transform keyTransform = key.transform;
            keyTransform.parent = transform;

            Vector3 newPos = keyTransform.localPosition;
            newPos.x = 0.4f + (KEY_DIMENTIONS * ((codeQueue.Count - 1) - countSize));
            newPos.y = KEY_DIMENTIONS * -1;

            keyTransform.localPosition = newPos;
            keyText.text = data.key.ToString();

            key.SetActive(countSize < size);
            keysQueue.Enqueue(key);

            countSize++;
        }

        Transform nextKey = keysQueue.Peek().transform;
        nextKey.SendMessage("PlayWithTag", "Next");
    }

    public void AddData(List<PointerData> dataList)
    {
        Debug.Log("AddData");

        int index = 0;

        while (index < dataList.Count)
        {
           /* codeQueue.Add(dataList[index]);

            bool haveKey = index < keys.Count;
            keys[index].gameObject.SetActive(haveKey);

            if (haveKey)
            {
                keys[index].text = dataList[index].key.ToString();
                keys[index].gameObject.SendMessage("ReverseWithTag", "Close");
            }
            */
            index++;
        }

    }


    public PointerData GetNextPointerData()
    {
        PointerData data = codeQueue.Dequeue();
        codeQueue.Enqueue(data);

        data.KeyQueuePos = MoveKeyQueue();

        return data;
    }

    private Vector3 MoveKeyQueue()
    {
        GameObject keyTaked = keysQueue.Dequeue();
        Transform keyTakedTransform = keyTaked.transform;

        keyTaked.SendMessage("StopWithTag", "Next");

        keyTaked.SetActive(false);
        keysQueue.Enqueue(keyTaked);

        Vector3 endQueuePos = keyTakedTransform.localPosition;
        endQueuePos.x = -0.4f;
        keyTakedTransform.localPosition = endQueuePos;

        int countSize = 0;
        foreach (GameObject key in keysQueue)
        {
            Transform keyTransform = key.transform;
            key.SetActive(countSize < size);

            keyTransform.rotation = Quaternion.Euler(Vector3.zero);

            PotaTween moveTween = PotaTween.Create(key, 5);
            float nextPosX = QUEUE_END_POS_X + (KEY_DIMENTIONS * ((codeQueue.Count - 1) - countSize));
            moveTween.SetPosition(TweenAxis.X, keyTransform.localPosition.x, nextPosX, true);
            moveTween.SetDuration(0.5f);

            if(countSize == size)
            {
                moveTween.SetAlpha(0F,1F);
            }

            moveTween.Play();
            countSize++;
        }

        return keyTakedTransform.localPosition;
    }

    public void Show()
    {
        
    }

    public void TakeKey(KeyCode key)
    {
        /*int index = 0;
        while (index < keys.Count)
        {
            bool haveKey = index < dataList.Count;
            keys[index].gameObject.SetActive(haveKey);

            if (haveKey)
            {
                Debug.Log("NextKey: " + dataList[index].key.ToString());
                keys[index].text = dataList[index].key.ToString();
                keys[index].gameObject.SendMessage("ReverseWithTag", "Close");
            }

            index++;
        }*/
    }
}
