﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerSlider : MonoBehaviour {

    public bool Initiated
    {
        get
        {
            return initited;
        }
    }

    public bool NoKey
    {
        get
        {
            return noKey;
        }
    }

    public GameObject keyModel;
    public GameObject throwBottleObject;
    public Sprite scoreSprite;
    public Sprite baseSprite;
    public float time;

    private SpriteRenderer spriteRenderer;
    private Transform ownTransform;
    private float distance = 0;
    private float cumulativeTime;
    private Vector2 trailPoint;

    private PotaTween pressKeyTween;
    private PotaTween moveTween;
    private PotaTween resetTween;
    private PotaTween scaleTween;

    //private GameObject target;

    private KeyCode currentKey;
    private GameObject currentKeyObject;

    private TextMesh keyText;
    private bool initited = false;
    private float takeTragoPercent = 0.1f;

    private bool throwBottle = false;
    private float throwStep = 0;

    private bool noKey = true;
    private GameObject target;
    private Vector3 startPos;
    private GameObject drinkTutorial1;
    private GameObject drinkTutorial2;

    // Use this for initialization
    void Awake () {
        ownTransform = transform;
        spriteRenderer = GetComponent<SpriteRenderer>();
        startPos = ownTransform.localPosition;
        drinkTutorial1 = GameObject.Find("DrinkTutorial1");
        drinkTutorial2 = GameObject.Find("DrinkTutorial2");


    }
        
    private void CreateKeyObject(KeyCode key)
    {
        currentKeyObject = Instantiate(keyModel);
        Transform keyTransform = currentKeyObject.transform;
        keyText = currentKeyObject.GetComponent<TextMesh>();


        /*currentKey = key;

        keyText.text = currentKey.ToString();
        currentKeyObject.transform.parent = ownTransform;
        keyTransform.localPosition = Vector3.down * 1.8f;*/

    }


    public void Initiate(Vector2 trailPoint/*, KeyCode key*/)
    {
        ownTransform.localScale = Vector3.one;
        ownTransform.localPosition = startPos;

        this.trailPoint = trailPoint;

        //CreateKeyObject(key);

        moveTween = PotaTween.Create(gameObject, 1);
        pressKeyTween = PotaTween.Create(gameObject, 2);
        scaleTween = PotaTween.Create(gameObject, 3);

        ResetTweens();

        resetTween = PotaTween.Create(gameObject, 3);
        initited = true;
    }

    private void ResetTweens()
    {
        moveTween.SetPosition(TweenAxis.X, trailPoint.x, trailPoint.y, true);
        moveTween.SetDuration(time);
        moveTween.SetEaseEquation(Ease.Equation.InSine);

        scaleTween.SetScale(Vector3.one * 1f, Vector3.zero);
        scaleTween.SetEaseEquation(Ease.Equation.InBack);
        scaleTween.SetDuration(0.3f);

        pressKeyTween.SetScale(Vector3.one * 1f, Vector3.one * 1.2f);
        pressKeyTween.SetEaseEquation(Ease.Equation.OutBack);
        pressKeyTween.SetLoop(LoopType.PingPong);
        pressKeyTween.LoopsNumber = 2;
        pressKeyTween.SetDuration(0.25f);
    }

    void Update () {
        if ((Input.anyKeyDown && !Input.GetKeyDown(currentKey)))
        {
            WrongPressing();
        }

        if (Input.GetKeyDown(currentKey))
        {
            if(target != null)
            {
                RightPressing();
            }
            

        }
        
    }

    private void WrongPressing()
    {
        /*moveTween.Stop();
        currentKeyObject.transform.parent = ownTransform.parent;
        currentKeyObject.SendMessage("PlayWithTag", "Fade");*/

       // ResetPointer();
        //SendMessageUpwards(DrinkMessages.WRONG_PRESSING);

    }


    private void RightPressing()
    {
        moveTween.Stop();
        pressKeyTween.Play();
        //currentKeyObject.SendMessage("StopWithTag", "Press");
        currentKeyObject.SendMessage("PlayWithTag", "Close");

        //target.SendMessage("Reverse");

        //target.SetActive(false);
        //target = null;

        Invoke("TakeGulp", 0.5f);
    }

    private void TakeGulp()
    {
        ResetPointer();

        Invoke("Gulp", 0.5f);
    }

    public void StartMove(float duration)
    {
        moveTween.SetDuration(duration);
        moveTween.Play(EndMove);

    }

    public void EndMove()
    {
        noKey = true;
        throwBottleObject.SetActive(true);
        gameObject.SetActive(false);

        Vector3 resetPos = ownTransform.localPosition;
        resetPos.x = trailPoint.x;
        ownTransform.localPosition = resetPos;

        spriteRenderer.sprite = baseSprite;

        SendMessageUpwards(DrinkMessages.LOST_POINTER);
    }

    public void ResetPointer()
    {
        resetTween.Clear();
        resetTween.SetPosition(TweenAxis.X, ownTransform.localPosition.x, trailPoint.x, true);
        resetTween.SetEaseEquation(Ease.Equation.InBack);
        resetTween.SetDuration(0.4f);
        resetTween.Play();
    }

    private void Gulp()
    {

        noKey = true;
        SendMessageUpwards(DrinkMessages.RIGHT_PRESSING);
        spriteRenderer.sprite = baseSprite;

        if (noKey)
        {
            resetTween.Clear();
            resetTween.SetAlpha(1,0);
            resetTween.SetDuration(0.4f);
            resetTween.Play(DisablePointer);
        }

    }

    private void DisablePointer()
    {
        gameObject.SetActive(false);
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Cup")
        {
            target = other.gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Cup" && target == other.gameObject)
        {
            target = null;
        }
    }

    private void OnMouseDrag()
    {
        if (moveTween)
        {
            moveTween.Stop();
        }

        if(drinkTutorial1) drinkTutorial1.SetActive(false);

        //Debug.Log(Camera.main.ScreenToWorldPoint(Input.mousePosition) + " - " + ownTransform.position);
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = ownTransform.position.z;
        ownTransform.position = mousePos;
    }
    
    private void OnMouseUp()
    {
        if (target)
        {
            if (drinkTutorial2)
            {
                drinkTutorial2.SetActive(false);
            }

            target = null;
            scaleTween.Play();
            initited = false;
            SendMessageUpwards(DrinkMessages.RIGHT_PRESSING);
        }
    }

    public void ChangePointerKey(PointerData data)
    {
        gameObject.SetActive(true);
        currentKeyObject.transform.parent = ownTransform;
        currentKeyObject.transform.localPosition = Vector3.down * 1.5f;

        noKey = false;

        currentKey = data.key;
        keyText.text = currentKey.ToString();
        currentKeyObject.SendMessage("Stop");

        PotaTween goToPointer = PotaTween.Create(currentKeyObject.gameObject, 5);
        goToPointer.SetPosition(data.KeyQueuePos, currentKeyObject.transform.position);
        goToPointer.SetDuration(0.3f);
        goToPointer.SetRotation(Vector3.zero, Vector3.forward * 360f);
        goToPointer.SetScale(Vector3.one * 0.6f, Vector3.one * 0.8f);
        goToPointer.SetAlpha(0f, 1f);

        goToPointer.Play();

        //currentKeyObject.SendMessage("ReverseWithTag", "Close");

    }

}
