﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrinkSlider : MonoBehaviour {

    public const float KEY_OFFSET = 0.85F;

    public List<PointerSlider> pointers;
    public List<GameObject> drinkClickAreaList;

    public Vector2 trailPoint;

    private Transform ownTransform;

    private PotaTween tween;
    private List<GameObject> keys;
    private List<KeyCode> sequence;
    private Queue<PointerData> dataQueue;

    private int currentPointer = 0;
    
    void Awake () {
        keys = new List<GameObject>();
        dataQueue = new Queue<PointerData>();
    }

    public void StartStep(/*NextKeysDisplay display,PointerData step*/)
    {

        /*while(index < step.pointerData.Count)
        {*/
            pointers[currentPointer].gameObject.SetActive(true);
            //dataQueue.Enqueue(display.GetNextPointerData());

            ActivePointer(/*display*/);
            currentPointer++;

            if(currentPointer >= pointers.Count)
            {
                currentPointer = 0;
            }
            //index++;
            /*}*/

    }

    public void ActivePointer(/*NextKeysDisplay display*/)
    {

        if (!pointers[currentPointer].Initiated)
        {
            pointers[currentPointer].Initiate(trailPoint);
            pointers[currentPointer].StartMove(2f);
        }

                

        /*while (index < pointers.Count)
        {
           // Debug.Log("GameStep: " + gameStep);
            //nextKeyDisplay.SetKey(steps[ClampGameStep(gameStep + 1)].pointerData);

            if (pointers[index].NoKey && pointers[index].isActiveAndEnabled)
            {
                PointerData data = display.GetNextPointerData();

                if (drinkClickAreaList.Count > 0)
                {
                    GameObject target = drinkClickAreaList[Random.Range(0, drinkClickAreaList.Count - 1)];
                    target.gameObject.SetActive(true);
                    target.SendMessage("Play");
                }

                if (!pointers[index].Initiated)
                {
                    pointers[index].Initiate(trailPoint/*, data.key);
                }

               // pointers[index].ChangePointerKey(data);
               // pointers[index].ChanceTakeTragoPercent(data.takeTragoPercent);

                pointers[index].StartMove(2f);

               /* if (display.IsEmpty)
                {
                }

                //SendMessageUpwards(DrinkMessages.POINTER_STARTED);

                //break;
            //}

            //index++;
        }*/



    }

    public void LostPointer()
    {
        if (NoPointerActive())
        {
            SendMessageUpwards(DrinkMessages.CLOSE_DRINK_GAME);
        }
    }

    public bool NoPointerActive()
    {
        for(int i = 0; i < pointers.Count; i++)
        {
            if (pointers[i].isActiveAndEnabled)
            {
                return false;
            }
        }

        return true;
    }

    public void NextKey(DrinkStep step)
    {
        /*sequenceStep++;
        pointer.ChangePointerKey(sequence[sequenceStep]);*/
    }


    private void FindTarget(GameObject target)
    {
        //this.target = target;
    }

    private void LostTarget()
    {
       // this.target = null;
    }

}
