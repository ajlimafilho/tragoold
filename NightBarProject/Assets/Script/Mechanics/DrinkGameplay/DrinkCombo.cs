﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrinkCombo : MonoBehaviour {

    public TextMesh textMesh;
    public RotationBehavior rotation;

    private int combo = 0;

    private PotaTween backTween;
    private PotaTween frontTween;

    void Start () {
        SetComboText();

        backTween = PotaTween.Create(rotation.gameObject);
        frontTween = PotaTween.Create(gameObject);

        SetupTweens();
    }

    private void SetupTweens()
    {
        backTween.Clear();
        backTween.SetScale(Vector3.one, Vector3.one * 0.6f);
        backTween.SetLoop(LoopType.PingPong);
        backTween.LoopsNumber = 2;
        backTween.SetDuration(0.5f);
        backTween.SetEaseEquation(Ease.Equation.OutBack);

        frontTween.Clear();
        frontTween.SetScale(Vector3.one, Vector3.one * 1.4f);
        frontTween.SetLoop(LoopType.PingPong);
        frontTween.LoopsNumber = 2;
        frontTween.SetDuration(0.5f);
        frontTween.SetEaseEquation(Ease.Equation.OutBack);

    }


    void Update () {
		
	}

    public void ResetCombo()
    {
        //rotation.StopRotation();

        backTween.Clear();
        backTween.SetRotation(Vector3.zero, Vector3.forward * 360f);
        backTween.SetDuration(1f);
        backTween.SetEaseEquation(Ease.Equation.OutBack);


        frontTween.Clear();
        frontTween.SetScale(Vector3.one * 0.6f, Vector3.one);
        frontTween.SetDuration(0.5f);

        backTween.Play();
        frontTween.Play();

        combo = 0;
        SetComboText();
    }


    public void Add()
    {
        combo++;
        SetComboText();

        frontTween.Play();
        backTween.Play();
    }

    private void SetComboText() {
        textMesh.text = combo.ToString();

        if (textMesh.text.Length == 1)
        {
            textMesh.text = "0" + textMesh.text;
        }
    }

}
