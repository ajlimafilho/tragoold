﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PointerData {

    public Vector3 KeyQueuePos
    {
        get;set;
    }

    //public float duration = 5f;
    public KeyCode key;
    //public float takeTragoPercent = 0.1f;
    //public Transform drinkArea;

    //private Vector3 keyQueuePos;

}
