﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrinkGame : MonoBehaviour {


    public DrinkCup drinkCup;
    public DrinkCombo combo;
    public DrinkSlider slider;
    public TextMesh startDrinkText;

    public NextKeysDisplay nextKeyDisplay;

    public List<PointerData> steps;

    private float energyPercent = 1;
    private int gameStep = 0;

    private bool canStartNextStep = true;
    private IEnumerator waitToNextStep;
    private PotaTween entryAnimation;

    private PotaTween textStartPopTween;
    private int countIntro = 3;

    private void Awake()
    {
        entryAnimation = PotaTween.Create(gameObject);
        entryAnimation.SetPosition(transform.position - Vector3.left * GameManager.Instance.levelBounds.size.x, transform.position);
        entryAnimation.SetEaseEquation(Ease.Equation.OutBack);
        entryAnimation.SetDuration(0.7F);

        textStartPopTween = PotaTween.Create(startDrinkText.gameObject);

        //nextKeyDisplay.Setup(steps);
    }

	void Start () {
        //drinkCup.Gulp(energyPercent);
        entryAnimation.Play();
    }
	
    public void OpenGame()
    {
        gameObject.SetActive(true);
        GameManager.Instance.DrinkMode();

        countIntro = 3;
        Invoke("CountIntro",1f);
    }

    public void CountIntro()
    {
        if(countIntro > 0)
        {
            startDrinkText.text = StringTable.GetText("START_DRINK_ON") + countIntro;

            textStartPopTween.Clear();
            textStartPopTween.Stop();
            textStartPopTween.SetScale(Vector3.zero, Vector3.one);
            textStartPopTween.SetEaseEquation(Ease.Equation.OutBack);
            textStartPopTween.SetDuration(1f);

            countIntro--;

           // textStartPopTween.Play();

            Invoke("CountIntro",0.6F);

            return;
        }

        startDrinkText.gameObject.SetActive(false);

        Invoke("StartStep", 0.2f);
    }

	void Update () {
		
	}

    private void PointerRightPlaced()
    {
        //energyPercent -= TAKE_TRAGO_PERCENT;
        //Debug.Log("PointerRightPlaced "+ energyPercent);
        drinkCup.Gulp();

        if (drinkCup.IsFull)
        {
            
            return;
        }

        combo.Add();
        StartStep();
    }

    private void FillCupComplete()
    {
       StartStep();
    }

    private IEnumerator WaitToNextStep()
    {
        yield return new WaitForSeconds(0.2f);
        canStartNextStep = true;
    }

    private void PointerWrongPressing()
    {
        slider.SendMessage("Shake", 0.5f);
        drinkCup.SendMessage("Shake", 0.5f);
        combo.SendMessage(DrinkMessages.RESET_COMBO);

        Invoke("StartStep", 1f);
    }

    private void StartStep()
    {

        slider.StartStep();
        /*
        if (canStartNextStep)
        {

            canStartNextStep = false;

            waitToNextStep = WaitToNextStep();
            StartCoroutine(waitToNextStep);

            NextStep();
            return;
        }*/
    }

    private void CloseDrinkGame()
    {
        entryAnimation.Clear();
        entryAnimation = PotaTween.Create(gameObject);
        entryAnimation.SetPosition(transform.position, transform.position - Vector3.right * GameManager.Instance.levelBounds.size.x);
        entryAnimation.SetEaseEquation(Ease.Equation.InBack);
        entryAnimation.SetDuration(0.7F);

        entryAnimation.Play();

        GameManager.Instance.InteractableMode();
    }

    private void NextStep()
    {
        gameStep = ClampGameStep(gameStep + 1);
    }

    public void ChangeBottle(DrinkBottle bottle)
    {

    }

    private int ClampGameStep(int value)
    {
        if (value >= steps.Count)
        {
            value = 0;
        }

        return value;
    }
}
