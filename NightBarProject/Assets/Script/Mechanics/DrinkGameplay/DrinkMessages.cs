﻿using UnityEngine;
using System.Collections;

public class DrinkMessages {
    public static string NEXT_SEQUENCE_STEP = "NextSequenceStep";
    public static string RIGHT_PRESSING = "PointerRightPlaced";
    public static string WRONG_PRESSING = "PointerWrongPressing";
    public static string RESET_COMBO = "ResetCombo";
    public static string FILL_CUP_COMPLETE = "FillCupComplete";
    public static string POINTER_STARTED = "PointerStarted";
    public static string REMOVE_KEY = "RemoveKey";
    public static string LOST_POINTER = "LostPointer";
    public static string CLOSE_DRINK_GAME = "CloseDrinkGame";
}
