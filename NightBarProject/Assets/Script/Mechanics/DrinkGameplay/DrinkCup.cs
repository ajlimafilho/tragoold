﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrinkCup : MonoBehaviour {

    public bool IsFull
    {
        get
        {
            return drinkPercent == 1;
        }
    }

    public Transform liquidDrink;

    private PotaTween bounceTween;
    private PotaTween liquidTween;
    private PotaTween takeTween;
    private Transform ownTransform;
    private Transform parent;
    private float liquidHeight;
    private float drinkPercent;

    void Awake ()
    {
        ownTransform = transform;
        parent = ownTransform.parent;

        bounceTween = PotaTween.Create(parent.gameObject, 1);
        bounceTween.SetScale(Vector3.one * 1.2f, Vector3.one);
        bounceTween.SetEaseEquation(Ease.Equation.OutBack);

        liquidTween = PotaTween.Create(liquidDrink.gameObject,2);
        liquidHeight = liquidDrink.GetComponent<SpriteRenderer>().bounds.size.y / ownTransform.localScale.y;

        Vector3 newPos = Vector3.down * (liquidHeight);
        newPos.z = 1;

        liquidDrink.localPosition = newPos;
        drinkPercent = 0;

        takeTween = PotaTween.Create(gameObject,1);
    }
	
	void Update ()
    {
		
	}

    public void TakeCup()
    {
        takeTween.Clear();
        takeTween.SetPosition(TweenAxis.X, parent.position.x, -12);
        takeTween.SetDuration(1f);
        takeTween.SetEaseEquation(Ease.Equation.InBack);
        takeTween.Play(FillGlass);
    }

    private void FillGlass()
    {
        liquidDrink.gameObject.SetActive(true);
        takeTween.SetEaseEquation(Ease.Equation.OutBack);
        takeTween.Reverse();

        Vector3 newPos = Vector3.down * (liquidHeight);
        newPos.z = 1;

        liquidDrink.localPosition = newPos;
        drinkPercent = 0;

        Invoke("FillGlassComplete", takeTween.Duration);
    }

    private void FillGlassComplete()
    {
        SendMessageUpwards(DrinkMessages.FILL_CUP_COMPLETE);
    }

    public void Gulp()
    {
        DecreaseLiquid();
    }

    private void DecreaseLiquid()
    {
        drinkPercent += 0.2f;

        bounceTween.Stop();
        bounceTween.Play();

        Vector3 newPos = (Vector3.down * ((liquidHeight * (drinkPercent - 1))));
        newPos.z = 1;

        if (liquidTween)
        {
            liquidTween.Stop();
            liquidTween.Clear();
            liquidTween.SetPosition(TweenAxis.Y, liquidDrink.localPosition.y, (liquidHeight * (drinkPercent - 1)) ,true);
            liquidTween.SetDuration(0.5f);
            liquidTween.SetEaseEquation(Ease.Equation.OutBack);

            liquidTween.Play(CheckIfIsFull);
        }

        liquidDrink.localPosition = newPos;
    }

    private void CheckIfIsFull()
    {
        if (IsFull)
        {
            TakeCup();
        }
    }
}
