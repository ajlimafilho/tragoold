﻿using UnityEngine;
using System.Collections;

public class LoadingBar : MonoBehaviour {

    public Transform bar;

    public GameObject background;
    public GameObject trago;
    public GameObject loadingBar;
    public GameObject container;
    public GameObject setupLanguage;

    private AsyncOperation async;

    // Use this for initialization
    void Start () {
        IntroGameActions.PLAY_CREDITS = true;
    }

    public void StartLoading(int number)
    {
        WeeklySetup.CURRENT_LOAD_EPISODE = number;
        async = Application.LoadLevelAdditiveAsync(number);
    }
    
    // Update is called once per frame
    void Update () {
        if (async != null)
        {
            Vector3 newPos = bar.localPosition;
            newPos.x = (8 * async.progress) - 8;

            bar.localPosition = newPos;

            if (async.isDone)
            {
                async = null;
                EndAnimation();
            }
        }
    }

    private void EndAnimation()
    {
        PotaTween alphaBar = PotaTween.Create(loadingBar);
        alphaBar.SetAlpha(1f, 0f);
        alphaBar.SetEaseEquation(Ease.Equation.InSine);
        alphaBar.SetDuration(0.8f);

        alphaBar.Play(PlayAllTweenBeforeChangeScene);
    }

    private void PlayAllTweenBeforeChangeScene()
    {
        PotaTween tragoTween = PotaTween.Create(trago);
        Transform tragoTransform = trago.transform;
        tragoTween.SetPosition(TweenAxis.Y, tragoTransform.position.y, tragoTransform.position.y + 10);
        tragoTween.SetDuration(1f);
        tragoTween.SetDelay(0.8f);
        tragoTween.SetEaseEquation(Ease.Equation.InBack);

        PotaTween backgroundTween = PotaTween.Create(background);
        Vector3 backgrounPos = background.transform.position;
        backgroundTween.SetPosition(TweenAxis.Y, backgrounPos.y, backgrounPos.y - 10);
        backgroundTween.SetDuration(1f);
        backgroundTween.SetDelay(0.8f);
        backgroundTween.SetEaseEquation(Ease.Equation.InSine);

        setupLanguage.SetActive(false);

        backgroundTween.Play();
        tragoTween.Play(DestroyLoadingScene);
    }
    private void DestroyLoadingScene()
    {
        GameObject.Destroy(container);
    }
}
