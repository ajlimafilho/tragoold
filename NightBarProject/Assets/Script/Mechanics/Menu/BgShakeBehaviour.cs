﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgShakeBehaviour : MonoBehaviour {

    private PotaTween _shakeBg;
    
    void OnEnable ()
    {
        TapeBehaviour.EvtOnTriggerEnter += OnTriggerEnter_ShakeBg;

        _shakeBg = PotaTween.Create(gameObject);
        _shakeBg.SetPosition(TweenAxis.Y, transform.position.y + 5, transform.position.y);
        _shakeBg.SetEaseEquation(Ease.Equation.OutElastic);
    }
	
	void OnTriggerEnter_ShakeBg()
    {
        _shakeBg.Play();
	} 

    void Disable()
    {
        TapeBehaviour.EvtOnTriggerEnter -= OnTriggerEnter_ShakeBg;
    }
}
