﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TapeBehaviour : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Rigidbody2D Rigidbody2D;
    public Transform TapeTransform;
    public Transform TapeLaunchPosition;
    public float TimeToWaitBeforeThrowTape;
    public int TapeDay; 

    private float xStartForce;
    private float zStartRotation;

    public delegate void OnTriggerEnterDelegate();
    public static OnTriggerEnterDelegate EvtOnTriggerEnter;

    public delegate void OnPointerEnterDelegate(int tapeDay);
    public static OnPointerEnterDelegate EvtOnPointerEnter;

    void OnEnable()
    {
        xStartForce = UnityEngine.Random.Range(-.35f, .35f);
        zStartRotation = UnityEngine.Random.Range(-15, 15);
        StartCoroutine(WaitBeforeThrowTape());
    }

    IEnumerator WaitBeforeThrowTape()
    {
        yield return new WaitForSeconds(TimeToWaitBeforeThrowTape);
        TapeTransform.position = TapeLaunchPosition.position;
        TapeTransform.Rotate(new Vector3(0, 0, zStartRotation));
        Rigidbody2D.isKinematic = false;
        Rigidbody2D.AddForce(new Vector2(xStartForce * 2, -1), ForceMode2D.Force);
    }

    bool canStartReduceGravityScale;
    void OnTriggerEnter2D(Collider2D trigger)
    {
        canStartReduceGravityScale = true;
        if(EvtOnTriggerEnter != null) EvtOnTriggerEnter();
    }

    private void Update()
    {
        if (!canStartReduceGravityScale || Rigidbody2D.gravityScale <= 1) return;

        Rigidbody2D.gravityScale--;

        if (Rigidbody2D.gravityScale <= 1)
            canStartReduceGravityScale = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        EvtOnPointerEnter(TapeDay);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        EvtOnPointerEnter(0);
    }
}
