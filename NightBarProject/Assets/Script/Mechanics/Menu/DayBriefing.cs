﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayBriefing : MonoBehaviour
{
    public Text BriefingText;

    void OnEnable ()
    {
        TapeBehaviour.EvtOnPointerEnter += ShowDayBriefing;
    }
    void OnDisable()
    {
        TapeBehaviour.EvtOnPointerEnter -= ShowDayBriefing;
    }

    void ShowDayBriefing (int day)
    {
		switch(day)
        {
            case 1:
                BriefingText.text = "day 1"; 
                break;
            case 2:
                BriefingText.text = "day 2";
                break;
            case 3:
                BriefingText.text = "day 3";
                break;
            case 4:
                BriefingText.text = "day 4";
                break;
            default:
                BriefingText.text = "";
                break;
        }
	}
}
