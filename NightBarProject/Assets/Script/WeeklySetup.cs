﻿using UnityEngine;
using System.Collections;

public class WeeklySetup : MonoBehaviour {

    public static string CONTINUE_KEY = "";
    public static int CONTINUE_COUNT = 3;
    public static int CURRENT_LOAD_EPISODE = 0;
    public static bool GAME_JOLT = false;
    public static bool WEEKLY_ON = false;

    public int continues = 3;

    public GameObject checkTime;
    public GameObject loadingBar;
    public GameObject connection;
    public GameObject weeklyMenu;

    public GameObject gameJoltAPI;

    void Start () 
    {
        WeeklySetup.WEEKLY_ON = true;

        if (WeeklySetup.GAME_JOLT)
        {
            GameJoltSetup();
            return;
        }
            
        checkTime.SetActive(true);
        connection.SetActive(true);
    }

    private void GameJoltSetup()
    {
        gameJoltAPI.SetActive(true);

        bool isSignedIn = GameJolt.API.Manager.Instance.CurrentUser != null;

        if (isSignedIn)
        {
            checkTime.SetActive(true);
            connection.SetActive(true);
            return;
        }

        GameJolt.UI.Manager.Instance.ShowSignIn((bool success) =>
        {

            if (success)
            {
                checkTime.SetActive(true);
                connection.SetActive(true);
            }
            else
            {
                Application.Quit();
            }

        });
    }

    public static void UpdateDate(System.DateTime dateTime)
    {
        if (WeeklySetup.GAME_JOLT)
        {
            GameJoltGetDate(dateTime);
        }

        GetLocalDate(dateTime);
    }

    private static void GetLocalDate(System.DateTime dateTime)
    {
        if (PlayerPrefs.HasKey("MyKey"))
        {
            int continues = PlayerPrefs.GetInt("MyKey");

            return;
        }

    }

    public static void SaveScoreData(int scoreValue, int tableID)
    {
        if (WeeklySetup.GAME_JOLT)
        {
            string scoreText = "You made " + scoreValue.ToString() + " points!";  // A string representing the score to be shown on the website.
            string extraData = ""; // This will not be shown on the website. You can store any information.

            GameJolt.API.Scores.Add(scoreValue, scoreText, tableID, extraData, (bool success) =>
            {
                Debug.Log(string.Format("Score Add {0}.", success ? "Successful" : "Failed"));
            });

            GameJolt.API.DataStore.Set("IS_MONEY_ABLE", "" + GameManager.IS_MONEY_ABLE, false);

            GameJolt.API.DataStore.Set("TICKET_ON", "" + GameManager.TICKET_ON, false);
        }
    }

    public static int TakeContinue()
    {
        WeeklySetup.CONTINUE_COUNT = Mathf.Max(WeeklySetup.CONTINUE_COUNT - 1, 0);

        if (WeeklySetup.GAME_JOLT)
        {
            //TODO: Rever funcionamento da Session;
            GameJolt.API.Sessions.Close();

            GameJolt.API.DataStore.Set("CONTINUES", WeeklySetup.CONTINUE_KEY + WeeklySetup.CONTINUE_COUNT.ToString(), false, (bool success) =>
            {
                if (success)
                {
                }
            });
        }

        return WeeklySetup.CONTINUE_COUNT;
    }

    private static void GameJoltGetDate(System.DateTime dateTime)
    {
        bool clearData = false;
        GameJolt.API.DataStore.Get("CONTINUES", false, (string continueData) =>
        {
            string newKey = dateTime.DayOfWeek.ToString() + "-" + dateTime.Day.ToString();

            if (continueData != null && !clearData)
            {
                string[] data = continueData.Split(new char[] { 'C' });

                if (data[0] == newKey)
                {
                    WeeklySetup.CONTINUE_KEY = data[0] + "C";
                    WeeklySetup.CONTINUE_COUNT = int.Parse(data[1]);
                    return;
                }
            }

            WeeklySetup.CONTINUE_KEY = newKey + "C";
            WeeklySetup.CONTINUE_COUNT = 3;

            GameJolt.API.DataStore.Set("CONTINUES", (WeeklySetup.CONTINUE_KEY + WeeklySetup.CONTINUE_COUNT), false);
        });


        GameJolt.API.DataStore.Get("TICKET_ON", false, (string continueData) =>
        {
            GameManager.TICKET_ON = (continueData == "true");
        });

        GameJolt.API.DataStore.Get("IS_MONEY_ABLE", false, (string continueData) =>
        {
            GameManager.IS_MONEY_ABLE = (continueData == "true");
        });
    }
    
    void Update () 
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
