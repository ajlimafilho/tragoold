﻿using UnityEngine;
using System.Collections;

public class PremiumSetup : MonoBehaviour {

    public static bool SKIP_BASIC_SETUP = false;

    public GameObject language;
    public GameObject days;
    public GameObject ruDays;
    public GameObject newGame;

	
	void Awake ()
    {
        language.SetActive(!SKIP_BASIC_SETUP);
        days.SetActive(SKIP_BASIC_SETUP);
    }
	
	void Update () {
	
	}
}
