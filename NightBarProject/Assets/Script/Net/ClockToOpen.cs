﻿using UnityEngine;
using System.Collections;

public class ClockToOpen : MonoBehaviour {


    public GameObject gameJolt;
    public GameObject liquidBar;

    private System.DateTime clock;
    private TextMesh textMesh;
    private string CountOneMinute;

    void Start () 
    {
        if(ConnectionHelper.Instance == null)
        {
            gameObject.SetActive(false);
            return;
        }

        int count = 0;
        bool foundNextDay = false;

        System.DateTime dateTime = ConnectionHelper.Instance.DateTime;
        System.DateTime nextDateTime = dateTime;

        while (!foundNextDay)
        {
            nextDateTime = nextDateTime.AddDays(1);
            foundNextDay = OpeningCalendar.CheckDayIsOpened(nextDateTime.DayOfWeek.ToString());
        }

        int daysToOpen = nextDateTime.Day - dateTime.Day;
        textMesh = GetComponent<TextMesh>();
        
        bool canShowClock = daysToOpen < 2;

        if (canShowClock)
        {
            int sec = (86400 * daysToOpen) - ((nextDateTime.Hour * 60) * 60) - (nextDateTime.Minute * 60) - nextDateTime.Second;

            clock = new System.DateTime();
            clock = clock.AddSeconds(sec);
            textMesh.text = clock.TimeOfDay.ToString();
            Invoke("CountOneSec", 1);
            return;
        }

        textMesh.text = daysToOpen +" "+ StringTable.GetText("DAYS");

    }

    private void CountOneSec()
    {
        try
        {
            clock = clock.AddSeconds(-1);
            textMesh.text = clock.TimeOfDay.ToString();
            Invoke("CountOneSec", 1);
        }
        catch(System.Exception)
        {
            gameJolt.SetActive(false);
            liquidBar.SetActive(true);
            liquidBar.SendMessage("StartLoading");
        }
        
        
    }
}
