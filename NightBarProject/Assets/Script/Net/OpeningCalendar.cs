﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OpeningCalendar {

    public static bool CheckDayIsOpened (string dayName) {

        Dictionary<string, bool> dayOpened = new Dictionary<string, bool>();

        //dayName = "Wednesday";

        dayOpened.Add("Monday", true);
        dayOpened.Add("Tuesday", false);
        dayOpened.Add("Wednesday", false);
        dayOpened.Add("Thursday", true);
        dayOpened.Add("Friday", true);
        dayOpened.Add("Saturday", true);
        dayOpened.Add("Sunday", true);

        return dayOpened[dayName];
    }
    
}
