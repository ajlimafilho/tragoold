﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;

public class ConnectionHelper : MonoBehaviour {

    #region Singleton
    private static ConnectionHelper instance;

    public static ConnectionHelper Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<ConnectionHelper>();

            return instance;
        }
    }
    #endregion

    public System.DateTime DateTime{
        get
        {
            return dateTime;
        }

        set
        {
            dateTime = value;
        }
    }

    public GameObject language;
    public GameObject tryButton;
    private System.DateTime dateTime;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        else Destroy(this); // or gameObject
    }

    void Start () 
    {
        Invoke("TryConnect",2);
    }

    public void TryAgainConnect()
    {
        GetComponent<TextMesh>().text = "Checking current date.";
        Invoke("TryConnect", 2);
    }

    public void TryConnect()
    {
        if (CheckConnection() == "Connection OK")
        {
            StartCoroutine(CheckTimeNET());
            return;
        }

        GetComponent<TextMesh>().text = "No internet connection.";
        tryButton.SetActive(true);
    }
    
    void Update () {
        
    }

    public string CheckConnection()
    {
        string html = string.Empty;
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://google.com");
        try
        {
            using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
            {
                bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                if (isSuccess)
                {
                    return "Connection OK";
                }
            }
        }
        catch
        {
            return "Connection Fail";
        }
        return html;
    }

    private IEnumerator CheckTimeNET()
    {
        WWW www = new WWW("http://www.timeapi.org/utc/now");
        yield return www;

        string data = www.data;

        dateTime = CreateDateTime(data);
        //dateTime = dateTime.AddDays(2);

        if (WeeklySetup.WEEKLY_ON)
        {
            WeeklySetup.UpdateDate(dateTime);
        }

        language.SetActive(true);
        SendMessage("Play");
    }

    private System.DateTime CreateDateTime(string data)
    {
        string[] words = data.Split(new char[] { 'T', '+' });

        string date = words[0];
        string hour = words[1];
        string utc = words[2];

        string[] dateList = date.Split(new char[] { '-' });
        string[] hourList = hour.Split(new char[]{':'});

        System.DateTime dateTime = new System.DateTime(int.Parse(dateList[0]), int.Parse(dateList[1]), int.Parse(dateList[2]));
        dateTime = dateTime.AddHours(int.Parse(hourList[0]));
        dateTime = dateTime.AddMinutes(int.Parse(hourList[1]));
        dateTime = dateTime.AddSeconds(int.Parse(hourList[2]));

        dateTime = dateTime.AddHours(UtcOffset(utc));

        //dateTime = dateTime.AddDays(1);

        return dateTime;
    }

    private void ClockToSec(string hour, int p)
    {
        
    }

    private int UtcOffset(string utc)
    {
        string localUtc = System.TimeZone.CurrentTimeZone.GetUtcOffset(System.DateTime.Now).ToString();
        localUtc = localUtc.Split(new char[] { ':' })[0];

        string worldUtc = utc.Split(new char[] { ':' })[0];
        int hourOffset = int.Parse(localUtc) - int.Parse(worldUtc);

        return hourOffset;
    }
}
